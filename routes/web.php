<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\HomeController;

Route::get('/', 'WelcomeController@index');
Route::get('/detail/{id}', 'WelcomeController@show');
Route::get('/login', function () {
    return view('auth.login');
});

Route::get('/registrasi_anggota', 'RegistrasiController@index');
Route::post('/registrasi_anggota', 'RegistrasiController@store');

Auth::routes();

Route::prefix('/admin')->middleware('role:Admin')->group(function () {
    /// Dashboard
    Route::get('/dashboard','HomeController@index');

    /// Akun
    Route::get('/master_akun', 'ChartAccountController@index');
    Route::get('/master_akun/add_akun', 'ChartAccountController@create');
    Route::post('/master_akun/add_akun', 'ChartAccountController@store');
    Route::get('/master_akun/edit_akun/{id}', 'ChartAccountController@edit');
    Route::post('/master_akun/update', 'ChartAccountController@update');

    /// SubAkun
    Route::get('/master_akun/add_subakun/{id}', 'ChartAccountController@create_subakun');
    Route::post('/master_akun/add_subakun', 'ChartAccountController@store_subakun');
    Route::get('/master_akun/edit_subakun/{id}', 'ChartAccountController@edit_subakun');
    Route::post('/master_akun/update_subakun', 'ChartAccountController@update_subakun');
    Route::post('/master_akun/hapus', 'ChartAccountController@destroy');


    /// Akt Mutasi KAS
    Route::get('/mutasi_kas', 'KasMutasiController@index');
    Route::get('/mutasi_kas/addnew', 'KasMutasiController@create');
    Route::post('/mutasi_kas/addnew', 'KasMutasiController@store');
    Route::post('/mutasi_kas/hapus', 'KasMutasiController@destroy');


    /// Akt Mutasi NON KAS
    
    Route::get('/memorial', 'MutasiNonKasController@index');
    Route::get('/memorial/addnew', 'MutasiNonKasController@create');
    Route::post('/memorial/addnew', 'MutasiNonKasController@store');
    


    /// Master Product
    Route::get('/master_product', 'ProductController@index');


    /// ANGGOTA
    Route::get('/master_anggota', 'MsAnggotaController@index');
    Route::get('/master_anggota/addnew', 'MsAnggotaController@create');
    Route::post('/master_anggota/addnew', 'MsAnggotaController@store');
    Route::post('/master_anggota/filter', 'MsAnggotaController@filter');
    Route::post('/master_anggota/verifikasi', 'MsAnggotaController@verifikasi');
    Route::get('/master_anggota/edit/{id}', 'MsAnggotaController@edit');
    Route::post('/master_anggota/update', 'MsAnggotaController@update');
    Route::post('/master_anggota/hapus', 'MsAnggotaController@destroy');
    Route::get('/master_anggota/detail/{id}', 'MsAnggotaController@show');
    Route::get('/master_anggota/kirim_email/{id}', 'RegistrasiController@sendemail');
    Route::get('/master_anggota/cetak/{id}', 'MsAnggotaController@cetakformulir');
    Route::get('/master_anggota/download/{id}', 'MsAnggotaController@download');








    /// SIMPANAN
    Route::get('/simp_master', 'SimpMasterController@index');
    Route::get('/simp_master/addnew', 'SimpMasterController@create');
    Route::post('/simp_master/addnew', 'SimpMasterController@store');
    Route::get('/simp_master/edit/{id}', 'SimpMasterController@edit');
    Route::post('/simp_master/update', 'SimpMasterController@update');
    Route::post('/simp_master/hapus', 'SimpMasterController@destroy');

    Route::get('/kirim_email', 'HomeController@sendemail');






    Route::get('/simp_rekening', 'SimpRekeningController@index');
    Route::get('/simp_rekening/addnew', 'SimpRekeningController@create');
    Route::post('/simp_rekening/addnew', 'SimpRekeningController@store');


    Route::post('/simp_rekening/filter', 'SimpRekeningController@filter');
    Route::get('/simp_rekening/lihat_mutasi/{id}', 'SimpRekeningController@show');

    Route::get('/simp_mutasi', 'SimpMutasiController@index');
    Route::get('/simp_mutasi/addnew', 'SimpMutasiController@create');
    Route::post('/simp_mutasi/addnew', 'SimpMutasiController@store');
    Route::post('/simp_mutasi/filter', 'SimpMutasiController@filter');
    Route::get('/simp_mutasi/download_excel', 'SimpMutasiController@export_excel');
    Route::post('/simp_mutasi/delete', 'SimpMutasiController@destroy');


    Route::get('/simp_mutasi/import', 'SimpMutasiController@import');
    Route::post('/simp_mutasi/import', 'SimpMutasiController@import_excel');




    /// PINJAMAN
    Route::get('/pby_master', 'PbyMasterController@index');
    Route::get('/pby_master/addnew', 'PbyMasterController@create');
    Route::post('/pby_master/addnew', 'PbyMasterController@store');
    Route::get('/pby_master/edit/{id}', 'PbyMasterController@edit');
    Route::post('/pby_master/update', 'PbyMasterController@update');
    Route::post('/pby_master/hapus', 'PbyMasterController@destroy');

    Route::get('/pengajuan', 'PbyPengajuanController@index');
    Route::get('/pengajuan/addnew', 'PbyPengajuanController@create');
    Route::post('/pengajuan/addnew', 'PbyPengajuanController@store');
    Route::get('/pengajuan/detail/{id}', 'PbyPengajuanController@show');
    Route::post('/pengajuan/update', 'PbyPengajuanController@update');

    Route::get('/pencairan', 'PbyPencairanController@index');
    Route::post('/pencairan/proses', 'PbyPencairanController@store');

    Route::get('/pby_rekening', 'PbyRekeningController@index');
    Route::get('/pby_rekening/detail/{id}', 'PbyRekeningController@show');

    Route::get('/pby_angsuran', 'PbyMutasiController@index');
    Route::post('/pby_angsuran/addnew','PbyMutasiController@store');

    Route::get('/pby_mutasi/download_excel', 'PbyMutasiController@export_excel');
    Route::post('/pby_mutasi/import', 'PbyMutasiController@import_excel');


    /// Laporan Akunting
    Route::get('/lap_akunting/lapjurnal', 'LapAkuntingController@jurnal');
    Route::get('/lap_akunting/bukubesar', 'LapAkuntingController@bukubesar_index');
    Route::post('/lap_akunting/bukubesar', 'LapAkuntingController@bukubesar_show');
    Route::get('/lap_akunting/labarugi', 'LapAkuntingController@labarugi_index');
    Route::post('/lap_akunting/labarugi', 'LapAkuntingController@labarugi_show');
    Route::get('/lap_akunting/aruskas', 'LapAkuntingController@aruskas_index');
    Route::post('/lap_akunting/aruskas', 'LapAkuntingController@aruskas_show');

    Route::get('/shu', 'ShuController@index');


    /// Produk 
    Route::get('/master_produk', 'ProdukController@index');
    Route::get('/master_produk/addnew', 'ProdukController@create');
    Route::post('/master_produk/addnew', 'ProdukController@store');
    Route::get('/master_produk/edit/{id}', 'ProdukController@edit');
    Route::post('/master_produk/update', 'ProdukController@update');


    Route::get('/master_produk/detail/{id}', 'ProdukController@show');    
    Route::post('/master_produk/hapus', 'ProdukController@destroy');

    /// Purchasing
    Route::get('/purchasing', 'PurchasingController@index');
    Route::get('/purchasing/detail/{id}', 'PurchasingController@show');
    Route::post('/purchasing/update', 'PurchasingController@update');



    /// MASTER
    Route::get('/ms_department', 'MsDepartmentController@index');
    Route::get('/ms_department/addnew', 'MsDepartmentController@create');
    Route::post('/ms_department/addnew', 'MsDepartmentController@store');
    Route::get('/ms_department/edit/{id}', 'MsDepartmentController@edit');
    Route::post('/ms_department/update', 'MsDepartmentController@update');
    Route::post('/ms_department/hapus', 'MsDepartmentController@destroy');


    Route::get('/ms_jabatan', 'MsJabatanController@index');
    Route::get('/ms_jabatan/addnew', 'MsJabatanController@create');
    Route::post('/ms_jabatan/addnew', 'MsJabatanController@store');
    Route::get('/ms_jabatan/edit/{id}', 'MsJabatanController@edit');
    Route::post('/ms_jabatan/update', 'MsJabatanController@update');
    Route::post('/ms_jabatan/hapus', 'MsJabatanController@destroy');


    Route::get('/ms_perusahaan', 'MsPerusahaanController@index');
    Route::get('/ms_perusahaan/addnew', 'MsPerusahaanController@create');
    Route::post('/ms_perusahaan/addnew', 'MsPerusahaanController@store');
    Route::get('/ms_perusahaan/edit/{id}', 'MsPerusahaanController@edit');
    Route::post('/ms_perusahaan/update', 'MsPerusahaanController@update');
    Route::post('/ms_perusahaan/hapus', 'MsPerusahaanController@destroy');




    Route::get('/master_user', 'UsersController@index');
    Route::get('/master_user/addnew', 'UsersController@create');
    Route::post('/master_user/addnew', 'UsersController@store');
    Route::get('/master_user/edit/{id}', 'UsersController@edit');
    Route::post('/master_user/update', 'UsersController@update');
    Route::post('/master_user/hapus', 'UsersController@destroy');


    Route::get('/edit_profil', 'UsersController@edit_profil');
    Route::post('/edit_profil/update', 'UsersController@update_profil');






});

Route::prefix('/anggota')->middleware('role:Anggota')->group(function () {
    /// Dashboard
    Route::get('/dashboard', 'HomeController@anggota');

    Route::get('/pengajuan', 'AgtPinjamanController@index');
    Route::get('/pengajuan/addnew', 'AgtPinjamanController@create');
    Route::post('/pengajuan/addnew', 'AgtPinjamanController@store');

    Route::get('/pinjaman', 'AgtPinjamanController@datarekening');
    Route::get('/pinjaman/detail/{id}', 'AgtPinjamanController@show');
    Route::get('/pinjaman/simulasi', 'AgtPinjamanController@simulasi_pinjaman');
    Route::post('/pinjaman/simulasi', 'AgtPinjamanController@proses_simulasi');



    Route::get('/simpanan','AgtSimpananController@index');
    Route::get('/simpanan/detail/{id}','AgtSimpananController@show');

    Route::get('/purchasing','AgtPurchasingController@index');
    // Route::get('/purchasing/{key}/{id}','AgtPurchasingController@create');
    Route::get('/purchasing/addnew/{id}','AgtPurchasingController@create');
    Route::post('/purchasing/addnew','AgtPurchasingController@store');
    Route::get('/purchasing/catalog','AgtPurchasingController@catalog');





});

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
