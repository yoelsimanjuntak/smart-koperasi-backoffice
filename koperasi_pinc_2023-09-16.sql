# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.7.3-MariaDB)
# Database: koperasi_pinc
# Generation Time: 2023-09-16 14:20:52 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table akt_arsipshu
# ------------------------------------------------------------

DROP TABLE IF EXISTS `akt_arsipshu`;

CREATE TABLE `akt_arsipshu` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `tanggal` date DEFAULT NULL,
  `shu` double(15,2) NOT NULL DEFAULT 0.00,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `akt_arsipshu` WRITE;
/*!40000 ALTER TABLE `akt_arsipshu` DISABLE KEYS */;

INSERT INTO `akt_arsipshu` (`id`, `tanggal`, `shu`, `created_at`, `updated_at`)
VALUES
	(1,'2023-01-31',2500000.00,'2023-05-22 15:43:19','2023-05-22 15:43:19'),
	(2,'2023-02-28',5750000.00,'2023-05-22 15:43:25','2023-05-22 15:43:25'),
	(3,'2023-03-31',4500000.00,'2023-05-22 15:43:31','2023-05-22 15:43:31'),
	(4,'2023-04-30',7000000.00,'2023-05-22 15:43:37','2023-05-22 15:43:37'),
	(5,'2023-05-31',5250000.00,'2023-06-07 11:06:43','2023-06-07 11:06:43');

/*!40000 ALTER TABLE `akt_arsipshu` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table akt_mutasi
# ------------------------------------------------------------

DROP TABLE IF EXISTS `akt_mutasi`;

CREATE TABLE `akt_mutasi` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `tanggal` date DEFAULT NULL,
  `no_bukti` varchar(30) NOT NULL DEFAULT '',
  `kde_akun` varchar(20) NOT NULL DEFAULT '',
  `keterangan` varchar(255) NOT NULL DEFAULT '',
  `debet` double(15,2) NOT NULL DEFAULT 0.00,
  `kredit` double(15,2) NOT NULL DEFAULT 0.00,
  `jns_mutasi` varchar(35) NOT NULL DEFAULT '',
  `user_id` int(10) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `akt_mutasi` WRITE;
/*!40000 ALTER TABLE `akt_mutasi` DISABLE KEYS */;

INSERT INTO `akt_mutasi` (`id`, `tanggal`, `no_bukti`, `kde_akun`, `keterangan`, `debet`, `kredit`, `jns_mutasi`, `user_id`, `created_at`, `updated_at`)
VALUES
	(5,'2023-05-11','012305110003','00110201','Modal Awal',100000000.00,0.00,'NonKas',1,'2023-05-11 10:26:39','2023-05-11 10:26:39'),
	(6,'2023-05-11','012305110003','00120204','Modal Awal',0.00,100000000.00,'NonKas',1,'2023-05-11 10:26:39','2023-05-11 10:26:39'),
	(7,'2023-05-11','012305110004','001101','Tarik Tunai Bank',50000000.00,0.00,'MutasiKas',1,'2023-05-11 10:45:22','2023-05-11 10:45:22'),
	(8,'2023-05-11','012305110004','00110201','Tarik Tunai Bank',0.00,50000000.00,'MutasiKas',1,'2023-05-11 10:45:22','2023-05-11 10:45:22'),
	(41,'2023-05-24','012305240002','001101','Setoran Simpanan Wajib A.n Adi Purwanto',150000.00,0.00,'Simpanan',1,'2023-05-24 13:34:22','2023-05-24 13:34:22'),
	(42,'2023-05-24','012305240002','00120202','Setoran Simpanan Wajib A.n Adi Purwanto',0.00,150000.00,'Simpanan',1,'2023-05-24 13:34:22','2023-05-24 13:34:22'),
	(43,'2023-05-24','012305240003','001101','Setoran Simpanan Pokok A.n Adi Purwanto',100000.00,0.00,'Simpanan',1,'2023-05-24 15:08:11','2023-05-24 15:08:11'),
	(44,'2023-05-24','012305240003','00120201','Setoran Simpanan Pokok A.n Adi Purwanto',0.00,100000.00,'Simpanan',1,'2023-05-24 15:08:11','2023-05-24 15:08:11'),
	(45,'2023-05-24','012305240004','00110301','Pencairan Pinjaman A.n Adi Purwanto',10000000.00,0.00,'Pencairan',1,'2023-05-24 15:32:07','2023-05-24 15:32:07'),
	(46,'2023-05-24','012305240004','001101','Pencairan Pinjaman A.n Adi Purwanto',0.00,10000000.00,'Pencairan',1,'2023-05-24 15:32:07','2023-05-24 15:32:07'),
	(47,'2023-05-24','012305240004','001101','Pendptan Adm Pinjaman A.n Adi Purwanto',50000.00,0.00,'AdmPencairan',1,'2023-05-24 15:32:07','2023-05-24 15:32:07'),
	(48,'2023-05-24','012305240004','00140102','Pendptan Adm Pinjaman A.n Adi Purwanto',0.00,50000.00,'AdmPencairan',1,'2023-05-24 15:32:07','2023-05-24 15:32:07'),
	(49,'2023-05-24','012305240005','00110302','Pencairan Pinjaman A.n Adi Purwanto',21000000.00,0.00,'Pencairan',1,'2023-05-24 15:54:31','2023-05-24 15:54:31'),
	(50,'2023-05-24','012305240005','001101','Pencairan Pinjaman A.n Adi Purwanto',0.00,21000000.00,'Pencairan',1,'2023-05-24 15:54:31','2023-05-24 15:54:31'),
	(51,'2023-05-24','012305240005','001101','Pendptan Adm Pinjaman A.n Adi Purwanto',0.00,0.00,'AdmPencairan',1,'2023-05-24 15:54:31','2023-05-24 15:54:31'),
	(52,'2023-05-24','012305240005','00140103','Pendptan Adm Pinjaman A.n Adi Purwanto',0.00,0.00,'AdmPencairan',1,'2023-05-24 15:54:31','2023-05-24 15:54:31'),
	(53,'2023-06-07','012306070001','001101','Angsuran Pinjaman A.n Adi Purwanto',868333.00,0.00,'Angsuran',1,'2023-06-07 10:28:12','2023-06-07 10:28:12'),
	(54,'2023-06-07','012306070001','00110301','Angsuran Pinjaman A.n Adi Purwanto',0.00,833333.00,'Angsuran',1,'2023-06-07 10:28:12','2023-06-07 10:28:12'),
	(55,'2023-06-07','012306070001','00140101','Angsuran Pinjaman A.n Adi Purwanto',0.00,35000.00,'Angsuran',1,'2023-06-07 10:28:12','2023-06-07 10:28:12'),
	(56,'2023-06-07','012306070002','001101','Lain lain',1000000.00,0.00,'MutasiKas',1,'2023-06-07 10:29:16','2023-06-07 10:29:16'),
	(57,'2023-06-07','012306070002','001402','Lain lain',0.00,1000000.00,'MutasiKas',1,'2023-06-07 10:29:16','2023-06-07 10:29:16'),
	(58,'2023-06-08','012306080001','001101','Angsuran Pinjaman A.n Adi Purwanto',1750000.00,0.00,'Angsuran',1,'2023-06-08 14:33:59','2023-06-08 14:33:59'),
	(59,'2023-06-08','012306080001','00110302','Angsuran Pinjaman A.n Adi Purwanto',0.00,1750000.00,'Angsuran',1,'2023-06-08 14:33:59','2023-06-08 14:33:59'),
	(60,'2023-06-08','012306080001','00140102','Angsuran Pinjaman A.n Adi Purwanto',0.00,0.00,'Angsuran',1,'2023-06-08 14:33:59','2023-06-08 14:33:59'),
	(61,'2023-06-08','012306080002','001101','Setoran Simpanan Pokok A.n Pandega',100000.00,0.00,'Simpanan',1,'2023-06-08 15:12:02','2023-06-08 15:12:02'),
	(62,'2023-06-08','012306080002','00120201','Setoran Simpanan Pokok A.n Pandega',0.00,100000.00,'Simpanan',1,'2023-06-08 15:12:02','2023-06-08 15:12:02'),
	(63,'2023-06-08','012306080003','001101','Setoran Simpanan Wajib A.n Pandega',150000.00,0.00,'Simpanan',1,'2023-06-08 15:12:12','2023-06-08 15:12:12'),
	(64,'2023-06-08','012306080003','00120202','Setoran Simpanan Wajib A.n Pandega',0.00,150000.00,'Simpanan',1,'2023-06-08 15:12:12','2023-06-08 15:12:12'),
	(65,'2023-06-08','012306080004','00110301','Pencairan Pinjaman A.n Pandega',12000000.00,0.00,'Pencairan',1,'2023-06-08 15:26:56','2023-06-08 15:26:56'),
	(66,'2023-06-08','012306080004','001101','Pencairan Pinjaman A.n Pandega',0.00,12000000.00,'Pencairan',1,'2023-06-08 15:26:56','2023-06-08 15:26:56'),
	(67,'2023-06-08','012306080004','001101','Pendptan Adm Pinjaman A.n Pandega',60000.00,0.00,'AdmPencairan',1,'2023-06-08 15:26:56','2023-06-08 15:26:56'),
	(68,'2023-06-08','012306080004','00140102','Pendptan Adm Pinjaman A.n Pandega',0.00,60000.00,'AdmPencairan',1,'2023-06-08 15:26:56','2023-06-08 15:26:56'),
	(69,'2023-06-08','012306080005','001101','Angsuran Pinjaman A.n Pandega',1042000.00,0.00,'Angsuran',1,'2023-06-08 15:29:31','2023-06-08 15:29:31'),
	(70,'2023-06-08','012306080005','00110301','Angsuran Pinjaman A.n Pandega',0.00,1000000.00,'Angsuran',1,'2023-06-08 15:29:31','2023-06-08 15:29:31'),
	(71,'2023-06-08','012306080005','00140101','Angsuran Pinjaman A.n Pandega',0.00,42000.00,'Angsuran',1,'2023-06-08 15:29:31','2023-06-08 15:29:31'),
	(72,'2023-06-09','012306090001','001101','Angsuran Pinjaman A.n Adi Purwanto',835000.00,0.00,'Angsuran',1,'2023-06-09 08:42:17','2023-06-09 08:42:17'),
	(73,'2023-06-09','012306090001','00110301','Angsuran Pinjaman A.n Adi Purwanto',0.00,800000.00,'Angsuran',1,'2023-06-09 08:42:17','2023-06-09 08:42:17'),
	(74,'2023-06-09','012306090001','00140101','Angsuran Pinjaman A.n Adi Purwanto',0.00,35000.00,'Angsuran',1,'2023-06-09 08:42:17','2023-06-09 08:42:17'),
	(75,'2023-06-10','012306100001','001101','Angsuran Pinjaman A.n Adi Purwanto',535000.00,0.00,'Angsuran',1,'2023-06-10 08:34:51','2023-06-10 08:34:51'),
	(76,'2023-06-10','012306100001','00110301','Angsuran Pinjaman A.n Adi Purwanto',0.00,500000.00,'Angsuran',1,'2023-06-10 08:34:51','2023-06-10 08:34:51'),
	(77,'2023-06-10','012306100001','00140101','Angsuran Pinjaman A.n Adi Purwanto',0.00,35000.00,'Angsuran',1,'2023-06-10 08:34:51','2023-06-10 08:34:51'),
	(78,'2023-06-12','012306120001','001101','simpanan pokok a.n adi purwanto',300000.00,0.00,'Simpanan',1,'2023-06-12 08:38:01','2023-06-12 08:38:01'),
	(79,'2023-06-12','012306120001','00120201','simpanan pokok a.n adi purwanto',0.00,300000.00,'Simpanan',1,'2023-06-12 08:38:01','2023-06-12 08:38:01'),
	(80,'2023-06-12','012306120002','001101','Angsuran Pinjaman A.n Adi Purwanto',868333.00,0.00,'Angsuran',1,'2023-06-12 08:39:38','2023-06-12 08:39:38'),
	(81,'2023-06-12','012306120002','00110301','Angsuran Pinjaman A.n Adi Purwanto',0.00,833333.00,'Angsuran',1,'2023-06-12 08:39:38','2023-06-12 08:39:38'),
	(82,'2023-06-12','012306120002','00140101','Angsuran Pinjaman A.n Adi Purwanto',0.00,35000.00,'Angsuran',1,'2023-06-12 08:39:38','2023-06-12 08:39:38'),
	(83,'2023-06-13','012306130001','00110301','Pencairan Pinjaman A.n Adi Purwanto',5000000.00,0.00,'Pencairan',1,'2023-06-13 16:33:55','2023-06-13 16:33:55'),
	(84,'2023-06-13','012306130001','001101','Pencairan Pinjaman A.n Adi Purwanto',0.00,5000000.00,'Pencairan',1,'2023-06-13 16:33:55','2023-06-13 16:33:55'),
	(85,'2023-06-13','012306130001','001101','Pendptan Adm Pinjaman A.n Adi Purwanto',25000.00,0.00,'AdmPencairan',1,'2023-06-13 16:33:55','2023-06-13 16:33:55'),
	(86,'2023-06-13','012306130001','00140102','Pendptan Adm Pinjaman A.n Adi Purwanto',0.00,25000.00,'AdmPencairan',1,'2023-06-13 16:33:55','2023-06-13 16:33:55'),
	(87,'2023-07-03','012307030001','001101','Angsuran Pinjaman A.n Adi Purwanto',417500.00,0.00,'Angsuran',1,'2023-07-03 18:00:25','2023-07-03 18:00:25'),
	(88,'2023-07-03','012307030001','00110301','Angsuran Pinjaman A.n Adi Purwanto',0.00,400000.00,'Angsuran',1,'2023-07-03 18:00:25','2023-07-03 18:00:25'),
	(89,'2023-07-03','012307030001','00140101','Angsuran Pinjaman A.n Adi Purwanto',0.00,17500.00,'Angsuran',1,'2023-07-03 18:00:25','2023-07-03 18:00:25'),
	(90,'2023-07-03','012307030002','001101','Angsuran Pinjaman A.n Adi Purwanto',434167.00,0.00,'Angsuran',1,'2023-07-03 18:01:54','2023-07-03 18:01:54'),
	(91,'2023-07-03','012307030002','00110301','Angsuran Pinjaman A.n Adi Purwanto',0.00,416667.00,'Angsuran',1,'2023-07-03 18:01:54','2023-07-03 18:01:54'),
	(92,'2023-07-03','012307030002','00140101','Angsuran Pinjaman A.n Adi Purwanto',0.00,17500.00,'Angsuran',1,'2023-07-03 18:01:54','2023-07-03 18:01:54'),
	(93,'2023-07-03','012307030003','001101','Angsuran Pinjaman A.n Adi Purwanto',434167.00,0.00,'Angsuran',1,'2023-07-03 18:02:14','2023-07-03 18:02:14'),
	(94,'2023-07-03','012307030003','00110301','Angsuran Pinjaman A.n Adi Purwanto',0.00,416667.00,'Angsuran',1,'2023-07-03 18:02:14','2023-07-03 18:02:14'),
	(95,'2023-07-03','012307030003','00140101','Angsuran Pinjaman A.n Adi Purwanto',0.00,17500.00,'Angsuran',1,'2023-07-03 18:02:14','2023-07-03 18:02:14'),
	(96,'2023-07-03','012307030004','001101','Angsuran Pinjaman A.n Adi Purwanto',434167.00,0.00,'Angsuran',1,'2023-07-03 18:02:31','2023-07-03 18:02:31'),
	(97,'2023-07-03','012307030004','00110301','Angsuran Pinjaman A.n Adi Purwanto',0.00,416667.00,'Angsuran',1,'2023-07-03 18:02:31','2023-07-03 18:02:31'),
	(98,'2023-07-03','012307030004','00140101','Angsuran Pinjaman A.n Adi Purwanto',0.00,17500.00,'Angsuran',1,'2023-07-03 18:02:31','2023-07-03 18:02:31'),
	(99,'2023-07-03','012307030005','001101','Angsuran Pinjaman A.n Adi Purwanto',417500.00,0.00,'Angsuran',1,'2023-07-03 18:02:50','2023-07-03 18:02:50'),
	(100,'2023-07-03','012307030005','00110301','Angsuran Pinjaman A.n Adi Purwanto',0.00,400000.00,'Angsuran',1,'2023-07-03 18:02:50','2023-07-03 18:02:50'),
	(101,'2023-07-03','012307030005','00140101','Angsuran Pinjaman A.n Adi Purwanto',0.00,17500.00,'Angsuran',1,'2023-07-03 18:02:50','2023-07-03 18:02:50'),
	(102,'2023-07-03','012307030006','001101','Angsuran Pinjaman A.n Adi Purwanto',434167.00,0.00,'Angsuran',1,'2023-07-03 18:03:01','2023-07-03 18:03:01'),
	(103,'2023-07-03','012307030006','00110301','Angsuran Pinjaman A.n Adi Purwanto',0.00,416667.00,'Angsuran',1,'2023-07-03 18:03:01','2023-07-03 18:03:01'),
	(104,'2023-07-03','012307030006','00140101','Angsuran Pinjaman A.n Adi Purwanto',0.00,17500.00,'Angsuran',1,'2023-07-03 18:03:01','2023-07-03 18:03:01'),
	(105,'2023-07-03','012307030007','001101','Angsuran Pinjaman A.n Adi Purwanto',434167.00,0.00,'Angsuran',1,'2023-07-03 18:03:23','2023-07-03 18:03:23'),
	(106,'2023-07-03','012307030007','00110301','Angsuran Pinjaman A.n Adi Purwanto',0.00,416667.00,'Angsuran',1,'2023-07-03 18:03:23','2023-07-03 18:03:23'),
	(107,'2023-07-03','012307030007','00140101','Angsuran Pinjaman A.n Adi Purwanto',0.00,17500.00,'Angsuran',1,'2023-07-03 18:03:23','2023-07-03 18:03:23'),
	(108,'2023-07-03','012307030008','001101','Angsuran Pinjaman A.n Adi Purwanto',434167.00,0.00,'Angsuran',1,'2023-07-03 18:03:33','2023-07-03 18:03:33'),
	(109,'2023-07-03','012307030008','00110301','Angsuran Pinjaman A.n Adi Purwanto',0.00,416667.00,'Angsuran',1,'2023-07-03 18:03:33','2023-07-03 18:03:33'),
	(110,'2023-07-03','012307030008','00140101','Angsuran Pinjaman A.n Adi Purwanto',0.00,17500.00,'Angsuran',1,'2023-07-03 18:03:33','2023-07-03 18:03:33'),
	(111,'2023-07-03','012307030009','001101','Angsuran Pinjaman A.n Adi Purwanto',434167.00,0.00,'Angsuran',1,'2023-07-03 18:03:44','2023-07-03 18:03:44'),
	(112,'2023-07-03','012307030009','00110301','Angsuran Pinjaman A.n Adi Purwanto',0.00,416667.00,'Angsuran',1,'2023-07-03 18:03:44','2023-07-03 18:03:44'),
	(113,'2023-07-03','012307030009','00140101','Angsuran Pinjaman A.n Adi Purwanto',0.00,17500.00,'Angsuran',1,'2023-07-03 18:03:44','2023-07-03 18:03:44'),
	(114,'2023-07-03','012307030010','001101','Angsuran Pinjaman A.n Adi Purwanto',434167.00,0.00,'Angsuran',1,'2023-07-03 18:03:50','2023-07-03 18:03:50'),
	(115,'2023-07-03','012307030010','00110301','Angsuran Pinjaman A.n Adi Purwanto',0.00,416667.00,'Angsuran',1,'2023-07-03 18:03:50','2023-07-03 18:03:50'),
	(116,'2023-07-03','012307030010','00140101','Angsuran Pinjaman A.n Adi Purwanto',0.00,17500.00,'Angsuran',1,'2023-07-03 18:03:50','2023-07-03 18:03:50'),
	(117,'2023-07-03','012307030011','001101','Angsuran Pinjaman A.n Adi Purwanto',417500.00,0.00,'Angsuran',1,'2023-07-03 18:04:01','2023-07-03 18:04:01'),
	(118,'2023-07-03','012307030011','00110301','Angsuran Pinjaman A.n Adi Purwanto',0.00,400000.00,'Angsuran',1,'2023-07-03 18:04:01','2023-07-03 18:04:01'),
	(119,'2023-07-03','012307030011','00140101','Angsuran Pinjaman A.n Adi Purwanto',0.00,17500.00,'Angsuran',1,'2023-07-03 18:04:01','2023-07-03 18:04:01'),
	(120,'2023-07-03','012307030012','001101','Angsuran Pinjaman A.n Adi Purwanto',434163.00,0.00,'Angsuran',1,'2023-07-03 18:05:01','2023-07-03 18:05:01'),
	(121,'2023-07-03','012307030012','00110301','Angsuran Pinjaman A.n Adi Purwanto',0.00,416663.00,'Angsuran',1,'2023-07-03 18:05:01','2023-07-03 18:05:01'),
	(122,'2023-07-03','012307030012','00140101','Angsuran Pinjaman A.n Adi Purwanto',0.00,17500.00,'Angsuran',1,'2023-07-03 18:05:01','2023-07-03 18:05:01');

/*!40000 ALTER TABLE `akt_mutasi` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table akt_mutasirev
# ------------------------------------------------------------

DROP TABLE IF EXISTS `akt_mutasirev`;

CREATE TABLE `akt_mutasirev` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `tanggal` date DEFAULT NULL,
  `no_bukti` varchar(30) NOT NULL DEFAULT '',
  `kde_akun` varchar(20) NOT NULL DEFAULT '',
  `keterangan` varchar(255) NOT NULL DEFAULT '',
  `debet` double(15,2) NOT NULL DEFAULT 0.00,
  `kredit` double(15,2) NOT NULL DEFAULT 0.00,
  `jns_mutasi` varchar(35) NOT NULL DEFAULT '',
  `user_id` int(10) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `akt_mutasirev` WRITE;
/*!40000 ALTER TABLE `akt_mutasirev` DISABLE KEYS */;

INSERT INTO `akt_mutasirev` (`id`, `tanggal`, `no_bukti`, `kde_akun`, `keterangan`, `debet`, `kredit`, `jns_mutasi`, `user_id`, `created_at`, `updated_at`)
VALUES
	(39,'2023-05-24','012305240001','001101','Setoran Simpanan Pokok A.n Adi Purwanto',100000.00,0.00,'Simpanan',1,'2023-05-24 13:18:57','2023-05-24 13:18:57'),
	(40,'2023-05-24','012305240001','00120201','Setoran Simpanan Pokok A.n Adi Purwanto',0.00,100000.00,'Simpanan',1,'2023-05-24 13:18:57','2023-05-24 13:18:57');

/*!40000 ALTER TABLE `akt_mutasirev` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table chart_account
# ------------------------------------------------------------

DROP TABLE IF EXISTS `chart_account`;

CREATE TABLE `chart_account` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `jenis` varchar(255) NOT NULL DEFAULT '',
  `kde_akun` varchar(15) NOT NULL DEFAULT '',
  `nma_akun` varchar(255) NOT NULL DEFAULT '',
  `pos_akun` int(1) NOT NULL,
  `saldo_awal` double(15,2) NOT NULL DEFAULT 0.00,
  `debet` double(15,2) NOT NULL DEFAULT 0.00,
  `kredit` double(15,2) NOT NULL DEFAULT 0.00,
  `saldo_akhir` double(15,2) NOT NULL DEFAULT 0.00,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`,`kde_akun`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `chart_account` WRITE;
/*!40000 ALTER TABLE `chart_account` DISABLE KEYS */;

INSERT INTO `chart_account` (`id`, `jenis`, `kde_akun`, `nma_akun`, `pos_akun`, `saldo_awal`, `debet`, `kredit`, `saldo_akhir`, `created_at`, `updated_at`)
VALUES
	(1,'Aktiva','001101','KAS',1,0.00,0.00,0.00,0.00,'2023-05-03 00:53:38','2023-08-22 08:53:50'),
	(2,'Aktiva','001102','BANK',1,0.00,0.00,0.00,0.00,'2023-05-03 00:53:48','2023-05-03 00:53:48'),
	(3,'Aktiva','00110201','  Bank BCA',2,0.00,0.00,0.00,0.00,'2023-05-03 00:53:57','2023-07-11 19:43:50'),
	(4,'Aktiva','001103','PINJAMAN',1,0.00,0.00,0.00,0.00,'2023-05-03 00:55:44','2023-05-03 00:55:44'),
	(6,'Aktiva','001104','PERSEDIAAN',1,0.00,0.00,0.00,0.00,'2023-05-03 00:56:37','2023-05-03 00:56:37'),
	(7,'Aktiva','00110401','  Persediaan Barang',2,0.00,0.00,0.00,0.00,'2023-05-03 00:56:47','2023-05-03 00:56:47'),
	(8,'Aktiva','001105','INVENTARIS',1,0.00,0.00,0.00,0.00,'2023-05-03 00:57:15','2023-05-03 00:57:15'),
	(9,'Aktiva','00110501','  Tanah',2,0.00,0.00,0.00,0.00,'2023-05-03 00:57:30','2023-05-03 00:57:30'),
	(10,'Aktiva','00110502','  Bangunan',2,0.00,0.00,0.00,0.00,'2023-05-03 00:57:37','2023-05-03 00:57:37'),
	(12,'Pasiva','00120203','  Simpanan Sukarela',2,0.00,0.00,0.00,0.00,'2023-05-03 00:58:53','2023-05-11 10:44:44'),
	(13,'Pasiva','001202','MODAL',1,0.00,0.00,0.00,0.00,'2023-05-03 00:59:43','2023-05-03 00:59:43'),
	(14,'Pasiva','00120201','  Simpanan Pokok',2,0.00,0.00,0.00,0.00,'2023-05-03 00:59:54','2023-07-11 19:43:50'),
	(15,'Pasiva','00120202','  Simpanan Wajib',2,0.00,0.00,0.00,0.00,'2023-05-03 01:00:02','2023-07-11 19:43:50'),
	(16,'Pasiva','00120204','  Modal Penyertaan',2,0.00,0.00,0.00,0.00,'2023-05-03 01:10:28','2023-07-11 19:43:50'),
	(18,'Pasiva','001203','DANA CADANGAN',1,0.00,0.00,0.00,0.00,'2023-05-03 01:15:09','2023-05-03 01:15:09'),
	(19,'Pasiva','001204','SISA HASIL USAHA',1,0.00,0.00,0.00,0.00,'2023-05-03 01:17:47','2023-05-03 01:17:47'),
	(20,'Pasiva','00120401','  SHU Tahun Berjalan',2,0.00,0.00,0.00,0.00,'2023-05-03 01:18:04','2023-05-03 01:18:04'),
	(22,'Pendapatan','001401','PENDAPATAN OPERASIONAL UTAMA',1,0.00,0.00,0.00,0.00,'2023-05-03 01:18:35','2023-05-03 01:18:35'),
	(23,'Pendapatan','00140101','  Pendapatan Jasa/Bunga Pinjaman Anggota',2,0.00,0.00,0.00,0.00,'2023-05-03 01:19:49','2023-08-22 08:53:50'),
	(24,'Pendapatan','00140103','  Pendapatan Adm Pinjaman',2,0.00,0.00,0.00,0.00,'2023-05-03 01:19:57','2023-05-24 13:07:58'),
	(25,'Pendapatan','00140104','  Pendapatan Adm Simpanan',2,0.00,0.00,0.00,0.00,'2023-05-03 01:20:12','2023-05-03 01:20:12'),
	(26,'Pendapatan','00140105','  Pendapatan Provisi',2,0.00,0.00,0.00,0.00,'2023-05-03 01:20:22','2023-05-03 01:20:22'),
	(27,'Pendapatan','00140106','  Pendapatan Notariel',2,0.00,0.00,0.00,0.00,'2023-05-03 01:20:39','2023-05-03 01:20:39'),
	(28,'Pendapatan','001402','PENDAPATAN NON OPERASIONAL',1,0.00,0.00,0.00,0.00,'2023-05-03 01:21:08','2023-07-11 19:43:50'),
	(29,'Pendapatan','00140201','  Pendapatan Jasa/Bunga Bank',2,0.00,0.00,0.00,0.00,'2023-05-03 01:21:18','2023-05-03 01:21:18'),
	(30,'Pendapatan','00140202','  Pendapatan Sewa',2,0.00,0.00,0.00,0.00,'2023-05-03 01:21:25','2023-05-03 01:21:25'),
	(31,'Pendapatan','00140107','  Pendapatan Materai',2,0.00,0.00,0.00,0.00,'2023-05-03 01:22:35','2023-05-03 01:22:35'),
	(32,'Biaya','001501','BIAYA OPERASIONAL',1,0.00,0.00,0.00,0.00,'2023-05-03 01:23:57','2023-05-03 01:23:57'),
	(33,'Biaya','00150101','  Biaya Tenaga Kerja',2,0.00,0.00,0.00,0.00,'2023-05-03 01:24:18','2023-05-03 01:24:18'),
	(34,'Biaya','00150102','  Biaya Pengurus dan Pengawas',2,0.00,0.00,0.00,0.00,'2023-05-03 01:24:35','2023-05-03 01:24:35'),
	(35,'Biaya','00150103','  Biaya Konsumsi',2,0.00,0.00,0.00,0.00,'2023-05-03 01:24:57','2023-05-11 10:44:44'),
	(36,'Biaya','00150104','  Biaya Perkoperasian',2,0.00,0.00,0.00,0.00,'2023-05-03 01:25:07','2023-05-03 01:25:07'),
	(37,'Biaya','00150105','  Biaya Listrik, Air, Telpon',2,0.00,0.00,0.00,0.00,'2023-05-03 01:25:29','2023-05-03 01:25:29'),
	(38,'Biaya','00150106','  Biaya Alat Tulis Kantor',2,0.00,0.00,0.00,0.00,'2023-05-03 01:25:49','2023-05-03 01:25:49'),
	(39,'Biaya','00150107','  Biaya Sumbangan / Sosial',2,0.00,0.00,0.00,0.00,'2023-05-03 01:26:13','2023-05-03 01:26:13'),
	(40,'Biaya','00150108','  Biaya RAT',2,0.00,0.00,0.00,0.00,'2023-05-03 01:26:26','2023-05-03 01:26:26'),
	(41,'Biaya','00150109','  Biaya Transport',2,0.00,0.00,0.00,0.00,'2023-05-03 01:27:03','2023-05-03 01:27:03'),
	(42,'Biaya','00150110','  Biaya Keamanan dan Kebersihan',2,0.00,0.00,0.00,0.00,'2023-05-03 01:27:19','2023-05-03 01:27:19'),
	(43,'Biaya','001502','BIAYA NON OPERASIONAL',1,0.00,0.00,0.00,0.00,'2023-05-03 01:28:00','2023-05-03 01:28:00'),
	(44,'Biaya','00150201','  Biaya Adm Bank',2,0.00,0.00,0.00,0.00,'2023-05-03 01:28:22','2023-05-03 01:28:22'),
	(45,'Biaya','00150202','  Biaya Pajak Bank',2,0.00,0.00,0.00,0.00,'2023-05-03 01:28:35','2023-05-03 01:28:35'),
	(46,'Aktiva','00110302','  Cicilan Barang',2,0.00,0.00,0.00,0.00,'2023-05-24 12:51:46','2023-07-11 19:43:50'),
	(47,'Pendapatan','00140102','  Pendapatan Jasa/Bunga Cicilan Barang',2,0.00,0.00,0.00,0.00,'2023-05-24 12:52:53','2023-07-11 19:43:50'),
	(48,'Pendapatan','001403','ASET',1,0.00,0.00,0.00,0.00,'2023-06-10 06:37:03','2023-06-10 06:37:03'),
	(49,'Pasiva','00120205','  Simpanan Khusus',2,0.00,0.00,0.00,0.00,'2023-09-04 17:39:18','2023-09-04 17:39:18');

/*!40000 ALTER TABLE `chart_account` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table jb_order
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jb_order`;

CREATE TABLE `jb_order` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `no_trx` varchar(20) NOT NULL DEFAULT '',
  `tanggal` date DEFAULT NULL,
  `id_anggota` bigint(11) NOT NULL,
  `id_produk` bigint(11) NOT NULL,
  `hpp` double(15,2) NOT NULL DEFAULT 0.00,
  `harga` double(15,2) NOT NULL DEFAULT 0.00,
  `qty` int(5) NOT NULL DEFAULT 0,
  `jangka` int(3) NOT NULL DEFAULT 12,
  `pembayaran` enum('Cicilan','Bayar Penuh') NOT NULL DEFAULT 'Cicilan',
  `notes` varchar(255) NOT NULL DEFAULT '',
  `ket_batal` varchar(255) NOT NULL DEFAULT '',
  `status_order` enum('Dibatalkan','Menunggu Konfirmasi','Diproses','Siap Diambil','Selesai') NOT NULL DEFAULT 'Menunggu Konfirmasi',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `jb_order` WRITE;
/*!40000 ALTER TABLE `jb_order` DISABLE KEYS */;

INSERT INTO `jb_order` (`id`, `no_trx`, `tanggal`, `id_anggota`, `id_produk`, `hpp`, `harga`, `qty`, `jangka`, `pembayaran`, `notes`, `ket_batal`, `status_order`, `created_at`, `updated_at`)
VALUES
	(1,'T012305240001','2023-05-24',1,1,19500000.00,21000000.00,1,12,'Cicilan','','','Selesai','2023-05-24 15:40:02','2023-05-25 06:10:31');

/*!40000 ALTER TABLE `jb_order` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`id`, `migration`, `batch`)
VALUES
	(1,'2014_10_12_000000_create_users_table',1),
	(2,'2014_10_12_100000_create_password_resets_table',1),
	(3,'2019_02_07_082604_create_anggota_table',1),
	(4,'2019_02_07_082623_create_tabungan_table',1),
	(5,'2019_02_07_082624_create_setoran_table',1),
	(6,'2019_02_07_082626_create_penarikan_table',1),
	(7,'2019_02_07_082724_create_riwayat_tabungan_table',1),
	(8,'2019_02_07_082725_create_bunga_tabungan_table',1),
	(9,'2019_02_09_093543_add_tahun_to_bunga_tabungan_table',1);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ms_anggota
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ms_anggota`;

CREATE TABLE `ms_anggota` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `no_anggota` varchar(20) NOT NULL DEFAULT '',
  `nik` varchar(20) NOT NULL DEFAULT '',
  `nama_anggota` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `id_perusahaan` bigint(11) NOT NULL,
  `no_ktp` varchar(20) NOT NULL DEFAULT '',
  `id_department` bigint(11) NOT NULL,
  `tempat_lahir` varchar(100) NOT NULL DEFAULT '',
  `tgl_lahir` date DEFAULT NULL,
  `no_telpon` varchar(20) NOT NULL DEFAULT '',
  `kontak_darurat` varchar(20) DEFAULT '',
  `jenis_kelamin` varchar(35) NOT NULL DEFAULT '',
  `status_karyawan` enum('Permanen','Kontrak','Probation') DEFAULT NULL,
  `no_rekening` varchar(20) NOT NULL DEFAULT '',
  `id_jabatan` bigint(11) NOT NULL,
  `foto_ktp` longtext DEFAULT NULL,
  `alamat` varchar(255) NOT NULL DEFAULT '',
  `alamat_domisili` varchar(255) DEFAULT '',
  `status_keanggotaan` enum('Aktif','Non-Aktif','Menunggu') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `ms_anggota` WRITE;
/*!40000 ALTER TABLE `ms_anggota` DISABLE KEYS */;

INSERT INTO `ms_anggota` (`id`, `user_id`, `no_anggota`, `nik`, `nama_anggota`, `email`, `id_perusahaan`, `no_ktp`, `id_department`, `tempat_lahir`, `tgl_lahir`, `no_telpon`, `kontak_darurat`, `jenis_kelamin`, `status_karyawan`, `no_rekening`, `id_jabatan`, `foto_ktp`, `alamat`, `alamat_domisili`, `status_keanggotaan`, `created_at`, `updated_at`)
VALUES
	(1,18,'PNC0001','220184','Adi Purwanto','adi@pincgroup.id',1,'337500000',24,'Pekalongan','1993-07-14','0813555',NULL,'Laki-laki','Permanen','3435555',2,'1684909038236-img_ktp.png','Jl. Gajah Mada No. 4','Jl. Palem V','Aktif','2023-05-24 13:17:18','2023-05-26 13:55:46'),
	(2,30,'PNC0002','11029','Pandega','pandega@pantarei.id',1,'33666',24,'Jakarta','1992-07-12','0888','08999','Laki-laki','Permanen','3222',2,'1686211777944-img_ktp.png','Jakarta','Jakarta','Aktif','2023-06-08 15:09:38','2023-06-08 15:10:16');

/*!40000 ALTER TABLE `ms_anggota` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ms_department
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ms_department`;

CREATE TABLE `ms_department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `ms_department` WRITE;
/*!40000 ALTER TABLE `ms_department` DISABLE KEYS */;

INSERT INTO `ms_department` (`id`, `nama`, `created_at`, `updated_at`)
VALUES
	(3,'-',NULL,NULL),
	(4,'ACCOUNT',NULL,NULL),
	(5,'BOD KOONTJIE',NULL,NULL),
	(6,'BOD PMA',NULL,NULL),
	(7,'BOD W3P',NULL,NULL),
	(8,'CREATIVE',NULL,NULL),
	(9,'DIGITAL',NULL,NULL),
	(10,'HR',NULL,NULL),
	(11,'IMADI',NULL,NULL),
	(12,'INDOSAT B2B',NULL,NULL),
	(13,'INDOSAT B2C',NULL,NULL),
	(14,'MATA ANGIN',NULL,NULL),
	(15,'MEDIA TJIPTA PARAGON',NULL,NULL),
	(16,'MULTIBRAND',NULL,NULL),
	(17,'MULTIBRAND (ADIRA DLL)',NULL,NULL),
	(18,'MULTIBRAND 1',NULL,NULL),
	(19,'MULTIBRAND 1 & 2',NULL,NULL),
	(20,'MULTIBRAND 2',NULL,NULL),
	(21,'PANTAREI',NULL,NULL),
	(22,'PINC',NULL,NULL),
	(23,'STRATEGY',NULL,NULL),
	(24,'SUPPORT',NULL,NULL),
	(25,'SUPPORT PAN',NULL,NULL),
	(26,'SUPPORT PMA',NULL,NULL),
	(27,'TRI',NULL,NULL),
	(28,'W3P',NULL,NULL);

/*!40000 ALTER TABLE `ms_department` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ms_jabatan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ms_jabatan`;

CREATE TABLE `ms_jabatan` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL DEFAULT '',
  `simp_pokok` double(15,2) NOT NULL DEFAULT 0.00,
  `simp_wajib` double(15,2) NOT NULL DEFAULT 0.00,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `ms_jabatan` WRITE;
/*!40000 ALTER TABLE `ms_jabatan` DISABLE KEYS */;

INSERT INTO `ms_jabatan` (`id`, `nama`, `simp_pokok`, `simp_wajib`, `created_at`, `updated_at`)
VALUES
	(1,'Non Staff',100000.00,50000.00,'2023-05-04 13:33:15','2023-05-04 13:33:15'),
	(2,'Staff - Senior Officer',100000.00,150000.00,'2023-05-04 13:33:37','2023-05-04 13:33:37'),
	(3,'Manager - Senior Manager',100000.00,250000.00,'2023-05-04 13:33:53','2023-05-04 13:33:53'),
	(4,'Director',100000.00,500000.00,'2023-05-04 13:34:39','2023-05-04 13:34:39');

/*!40000 ALTER TABLE `ms_jabatan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ms_kategori
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ms_kategori`;

CREATE TABLE `ms_kategori` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `kategori` varchar(255) NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `ms_kategori` WRITE;
/*!40000 ALTER TABLE `ms_kategori` DISABLE KEYS */;

INSERT INTO `ms_kategori` (`id`, `kategori`, `created_at`, `updated_at`)
VALUES
	(1,'Sembako','2023-05-11 15:01:16','2023-05-11 15:01:16'),
	(2,'Gadget & Elektronik','2023-05-11 15:01:23','2023-05-11 15:01:23'),
	(3,'Furniture','2023-05-11 15:01:33','2023-05-11 15:01:33'),
	(4,'Kendaraan Bermotor','2023-05-11 15:01:38','2023-05-11 15:01:38');

/*!40000 ALTER TABLE `ms_kategori` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ms_perusahaan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ms_perusahaan`;

CREATE TABLE `ms_perusahaan` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `inisial` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `ms_perusahaan` WRITE;
/*!40000 ALTER TABLE `ms_perusahaan` DISABLE KEYS */;

INSERT INTO `ms_perusahaan` (`id`, `nama`, `inisial`, `created_at`, `updated_at`)
VALUES
	(1,'PINC','PNC','2023-05-04 12:27:08','2023-05-04 12:27:08'),
	(2,'Pantarei','PAN','2023-05-04 12:27:08','2023-05-04 12:27:08'),
	(3,'Mata Angin','PMA','2023-05-04 12:27:08','2023-05-31 14:15:47'),
	(4,'IMADI','IMD','2023-05-04 12:27:08','2023-05-04 12:27:08'),
	(5,'W3P','WTP','2023-05-04 12:27:08','2023-05-04 12:27:08'),
	(6,'TANGGA','TGG','2023-05-04 12:27:08','2023-05-04 12:27:08'),
	(7,'KOONTJIE','KNC','2023-05-04 12:27:08','2023-05-04 12:27:08'),
	(8,'MTP','MTP','2023-05-24 13:06:21','2023-05-24 13:06:21');

/*!40000 ALTER TABLE `ms_perusahaan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ms_produk
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ms_produk`;

CREATE TABLE `ms_produk` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `id_kategori` bigint(11) NOT NULL DEFAULT 0,
  `nama_barang` varchar(225) NOT NULL DEFAULT '',
  `deskripsi` varchar(255) NOT NULL DEFAULT '',
  `harga_beli` double(15,2) NOT NULL DEFAULT 0.00,
  `harga_jual` double(15,2) NOT NULL DEFAULT 0.00,
  `status` enum('PreOrder','Ready Stock','Discontinue','Out of Stock') NOT NULL DEFAULT 'PreOrder',
  `estimasi` int(8) NOT NULL DEFAULT 0,
  `cicilan` varchar(1) NOT NULL DEFAULT 'Y',
  `bayar_penuh` varchar(1) NOT NULL DEFAULT 'Y',
  `foto` longtext NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `ms_produk` WRITE;
/*!40000 ALTER TABLE `ms_produk` DISABLE KEYS */;

INSERT INTO `ms_produk` (`id`, `id_kategori`, `nama_barang`, `deskripsi`, `harga_beli`, `harga_jual`, `status`, `estimasi`, `cicilan`, `bayar_penuh`, `foto`, `created_at`, `updated_at`)
VALUES
	(1,2,'Macbook Pro M1','Macbook',19500000.00,21000000.00,'PreOrder',6,'Y','Y','1683877988685-QROsG0gbqAE8BIplZo6H2exI5WvJHs3t3TPae001.jpeg','2023-05-12 14:53:08','2023-05-12 14:53:08'),
	(3,3,'Meja Kerja L','meja kerja ukuran Large',2450000.00,2780000.00,'Ready Stock',0,'Y','N','1683884935885-Merk-Meja-Kantor-Terbaik.jpg','2023-05-12 16:48:55','2023-05-14 00:08:46'),
	(4,4,'Honda Beat Deluxe','Honda Beat',17850000.00,19000000.00,'PreOrder',12,'Y','Y','1684727235929-deluxe-green-3-01022023-085857.jpg','2023-05-19 23:15:44','2023-05-22 10:47:15'),
	(5,2,'IPhone 14 Pro','Iphone 14 Pro 512GB',18900000.00,20000000.00,'PreOrder',6,'Y','Y','1685338992211-Iphone.jpg','2023-05-29 12:43:12','2023-05-29 12:43:12');

/*!40000 ALTER TABLE `ms_produk` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table pby_import
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pby_import`;

CREATE TABLE `pby_import` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `id_norek` bigint(11) NOT NULL,
  `no_rek` varchar(25) NOT NULL DEFAULT '',
  `nama_anggota` varchar(255) NOT NULL DEFAULT '',
  `nama_pinjaman` varchar(100) NOT NULL DEFAULT '',
  `angs_pokok` double(15,2) NOT NULL DEFAULT 0.00,
  `angs_jasa` double(15,2) NOT NULL DEFAULT 0.00,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table pby_jadwal
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pby_jadwal`;

CREATE TABLE `pby_jadwal` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `id_norek` bigint(11) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `angske` int(4) NOT NULL DEFAULT 0,
  `angs_pokok` double(15,2) NOT NULL DEFAULT 0.00,
  `angs_jasa` double(15,2) NOT NULL DEFAULT 0.00,
  `status` varchar(5) NOT NULL DEFAULT '',
  `user_id` int(10) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `pby_jadwal` WRITE;
/*!40000 ALTER TABLE `pby_jadwal` DISABLE KEYS */;

INSERT INTO `pby_jadwal` (`id`, `id_norek`, `tanggal`, `angske`, `angs_pokok`, `angs_jasa`, `status`, `user_id`, `created_at`, `updated_at`)
VALUES
	(1,1,'2023-06-24',1,833333.00,35000.00,'OK',0,'2023-05-24 15:32:07','2023-05-24 15:32:07'),
	(2,1,'2023-07-24',2,833333.00,35000.00,'OK',0,'2023-05-24 15:32:07','2023-05-24 15:32:07'),
	(3,1,'2023-08-24',3,833333.00,35000.00,'OK',0,'2023-05-24 15:32:07','2023-05-24 15:32:07'),
	(4,1,'2023-09-24',4,833333.00,35000.00,'OK',0,'2023-05-24 15:32:07','2023-05-24 15:32:07'),
	(5,1,'2023-10-24',5,833333.00,35000.00,'',0,'2023-05-24 15:32:07','2023-05-24 15:32:07'),
	(6,1,'2023-11-24',6,833333.00,35000.00,'',0,'2023-05-24 15:32:07','2023-05-24 15:32:07'),
	(7,1,'2023-12-24',7,833333.00,35000.00,'',0,'2023-05-24 15:32:07','2023-05-24 15:32:07'),
	(8,1,'2024-01-24',8,833333.00,35000.00,'',0,'2023-05-24 15:32:07','2023-05-24 15:32:07'),
	(9,1,'2024-02-24',9,833333.00,35000.00,'',0,'2023-05-24 15:32:07','2023-05-24 15:32:07'),
	(10,1,'2024-03-24',10,833333.00,35000.00,'',0,'2023-05-24 15:32:07','2023-05-24 15:32:07'),
	(11,1,'2024-04-24',11,833333.00,35000.00,'',0,'2023-05-24 15:32:07','2023-05-24 15:32:07'),
	(12,1,'2024-05-24',12,833337.00,35000.00,'',0,'2023-05-24 15:32:07','2023-05-24 15:32:07'),
	(13,2,'2023-06-24',1,1750000.00,0.00,'OK',0,'2023-05-24 15:54:31','2023-05-24 15:54:31'),
	(14,2,'2023-07-24',2,1750000.00,0.00,'',0,'2023-05-24 15:54:31','2023-05-24 15:54:31'),
	(15,2,'2023-08-24',3,1750000.00,0.00,'',0,'2023-05-24 15:54:31','2023-05-24 15:54:31'),
	(16,2,'2023-09-24',4,1750000.00,0.00,'',0,'2023-05-24 15:54:31','2023-05-24 15:54:31'),
	(17,2,'2023-10-24',5,1750000.00,0.00,'',0,'2023-05-24 15:54:31','2023-05-24 15:54:31'),
	(18,2,'2023-11-24',6,1750000.00,0.00,'',0,'2023-05-24 15:54:31','2023-05-24 15:54:31'),
	(19,2,'2023-12-24',7,1750000.00,0.00,'',0,'2023-05-24 15:54:31','2023-05-24 15:54:31'),
	(20,2,'2024-01-24',8,1750000.00,0.00,'',0,'2023-05-24 15:54:31','2023-05-24 15:54:31'),
	(21,2,'2024-02-24',9,1750000.00,0.00,'',0,'2023-05-24 15:54:31','2023-05-24 15:54:31'),
	(22,2,'2024-03-24',10,1750000.00,0.00,'',0,'2023-05-24 15:54:31','2023-05-24 15:54:31'),
	(23,2,'2024-04-24',11,1750000.00,0.00,'',0,'2023-05-24 15:54:31','2023-05-24 15:54:31'),
	(24,2,'2024-05-24',12,1750000.00,0.00,'',0,'2023-05-24 15:54:31','2023-05-24 15:54:31'),
	(25,3,'2023-07-08',1,1000000.00,42000.00,'OK',0,'2023-06-08 15:26:56','2023-06-08 15:26:56'),
	(26,3,'2023-08-08',2,1000000.00,42000.00,'',0,'2023-06-08 15:26:56','2023-06-08 15:26:56'),
	(27,3,'2023-09-08',3,1000000.00,42000.00,'',0,'2023-06-08 15:26:56','2023-06-08 15:26:56'),
	(28,3,'2023-10-08',4,1000000.00,42000.00,'',0,'2023-06-08 15:26:56','2023-06-08 15:26:56'),
	(29,3,'2023-11-08',5,1000000.00,42000.00,'',0,'2023-06-08 15:26:56','2023-06-08 15:26:56'),
	(30,3,'2023-12-08',6,1000000.00,42000.00,'',0,'2023-06-08 15:26:56','2023-06-08 15:26:56'),
	(31,3,'2024-01-08',7,1000000.00,42000.00,'',0,'2023-06-08 15:26:56','2023-06-08 15:26:56'),
	(32,3,'2024-02-08',8,1000000.00,42000.00,'',0,'2023-06-08 15:26:56','2023-06-08 15:26:56'),
	(33,3,'2024-03-08',9,1000000.00,42000.00,'',0,'2023-06-08 15:26:56','2023-06-08 15:26:56'),
	(34,3,'2024-04-08',10,1000000.00,42000.00,'',0,'2023-06-08 15:26:56','2023-06-08 15:26:56'),
	(35,3,'2024-05-08',11,1000000.00,42000.00,'',0,'2023-06-08 15:26:56','2023-06-08 15:26:56'),
	(36,3,'2024-06-08',12,1000000.00,42000.00,'',0,'2023-06-08 15:26:56','2023-06-08 15:26:56'),
	(37,4,'2023-07-13',1,416667.00,17500.00,'OK',0,'2023-06-13 16:33:55','2023-06-13 16:33:55'),
	(38,4,'2023-08-13',2,416667.00,17500.00,'OK',0,'2023-06-13 16:33:55','2023-06-13 16:33:55'),
	(39,4,'2023-09-13',3,416667.00,17500.00,'OK',0,'2023-06-13 16:33:55','2023-06-13 16:33:55'),
	(40,4,'2023-10-13',4,416667.00,17500.00,'OK',0,'2023-06-13 16:33:55','2023-06-13 16:33:55'),
	(41,4,'2023-11-13',5,416667.00,17500.00,'OK',0,'2023-06-13 16:33:55','2023-06-13 16:33:55'),
	(42,4,'2023-12-13',6,416667.00,17500.00,'OK',0,'2023-06-13 16:33:55','2023-06-13 16:33:55'),
	(43,4,'2024-01-13',7,416667.00,17500.00,'OK',0,'2023-06-13 16:33:55','2023-06-13 16:33:55'),
	(44,4,'2024-02-13',8,416667.00,17500.00,'OK',0,'2023-06-13 16:33:55','2023-06-13 16:33:55'),
	(45,4,'2024-03-13',9,416667.00,17500.00,'OK',0,'2023-06-13 16:33:55','2023-06-13 16:33:55'),
	(46,4,'2024-04-13',10,416667.00,17500.00,'OK',0,'2023-06-13 16:33:55','2023-06-13 16:33:55'),
	(47,4,'2024-05-13',11,416667.00,17500.00,'OK',0,'2023-06-13 16:33:55','2023-06-13 16:33:55'),
	(48,4,'2024-06-13',12,416663.00,17500.00,'OK',0,'2023-06-13 16:33:55','2023-06-13 16:33:55');

/*!40000 ALTER TABLE `pby_jadwal` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pby_master
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pby_master`;

CREATE TABLE `pby_master` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `kode` varchar(2) NOT NULL DEFAULT '',
  `nama` varchar(100) NOT NULL DEFAULT '',
  `akun_produk` varchar(20) NOT NULL DEFAULT '',
  `akun_jasa` varchar(20) NOT NULL DEFAULT '',
  `akun_adm` varchar(20) NOT NULL DEFAULT '',
  `bya_adm` double(5,2) NOT NULL DEFAULT 0.00,
  `persen_jasa` double(5,2) NOT NULL DEFAULT 0.00,
  `status` varchar(1) NOT NULL DEFAULT 'Y',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `pby_master` WRITE;
/*!40000 ALTER TABLE `pby_master` DISABLE KEYS */;

INSERT INTO `pby_master` (`id`, `kode`, `nama`, `akun_produk`, `akun_jasa`, `akun_adm`, `bya_adm`, `persen_jasa`, `status`, `created_at`, `updated_at`)
VALUES
	(1,'50','Pinjaman Anggota','00110301','00140101','00140102',0.50,0.35,'Y','2023-05-04 12:50:28','2023-05-04 12:50:28'),
	(4,'51','Cicilan Barang','00110302','00140102','00140103',0.00,0.00,'Y','2023-05-24 12:51:04','2023-05-24 12:51:04');

/*!40000 ALTER TABLE `pby_master` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pby_mutasi
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pby_mutasi`;

CREATE TABLE `pby_mutasi` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `id_norek` bigint(11) NOT NULL,
  `no_bukti` varchar(20) NOT NULL DEFAULT '',
  `tanggal` date DEFAULT NULL,
  `no_rek` varchar(20) NOT NULL DEFAULT '',
  `keterangan` varchar(255) NOT NULL DEFAULT '',
  `angs_pokok` double(15,2) NOT NULL DEFAULT 0.00,
  `angs_jasa` double(15,2) NOT NULL DEFAULT 0.00,
  `user_id` int(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `pby_mutasi` WRITE;
/*!40000 ALTER TABLE `pby_mutasi` DISABLE KEYS */;

INSERT INTO `pby_mutasi` (`id`, `id_norek`, `no_bukti`, `tanggal`, `no_rek`, `keterangan`, `angs_pokok`, `angs_jasa`, `user_id`, `created_at`, `updated_at`)
VALUES
	(1,1,'012306070001','2023-06-07','0015000001','Angsuran Pinjaman A.n Adi Purwanto',833333.00,35000.00,1,'2023-06-07 10:28:12','2023-06-07 10:28:12'),
	(2,2,'012306080001','2023-06-08','0015100001','Angsuran Pinjaman A.n Adi Purwanto',1750000.00,0.00,1,'2023-06-08 14:33:59','2023-06-08 14:33:59'),
	(3,3,'012306080005','2023-06-08','0015000002','Angsuran Pinjaman A.n Pandega',1000000.00,42000.00,1,'2023-06-08 15:29:31','2023-06-08 15:29:31'),
	(4,1,'012306090001','2023-06-09','0015000001','Angsuran Pinjaman A.n Adi Purwanto',800000.00,35000.00,1,'2023-06-09 08:42:17','2023-06-09 08:42:17'),
	(5,1,'012306100001','2023-06-10','0015000001','Angsuran Pinjaman A.n Adi Purwanto',500000.00,35000.00,1,'2023-06-10 08:34:51','2023-06-10 08:34:51'),
	(6,1,'012306120002','2023-06-12','0015000001','Angsuran Pinjaman A.n Adi Purwanto',833333.00,35000.00,1,'2023-06-12 08:39:38','2023-06-12 08:39:38'),
	(7,4,'012307030001','2023-07-03','0015000003','Angsuran Pinjaman A.n Adi Purwanto',400000.00,17500.00,1,'2023-07-03 18:00:25','2023-07-03 18:00:25'),
	(8,4,'012307030002','2023-07-03','0015000003','Angsuran Pinjaman A.n Adi Purwanto',416667.00,17500.00,1,'2023-07-03 18:01:54','2023-07-03 18:01:54'),
	(9,4,'012307030003','2023-07-03','0015000003','Angsuran Pinjaman A.n Adi Purwanto',416667.00,17500.00,1,'2023-07-03 18:02:14','2023-07-03 18:02:14'),
	(10,4,'012307030004','2023-07-03','0015000003','Angsuran Pinjaman A.n Adi Purwanto',416667.00,17500.00,1,'2023-07-03 18:02:31','2023-07-03 18:02:31'),
	(11,4,'012307030005','2023-07-03','0015000003','Angsuran Pinjaman A.n Adi Purwanto',400000.00,17500.00,1,'2023-07-03 18:02:50','2023-07-03 18:02:50'),
	(12,4,'012307030006','2023-07-03','0015000003','Angsuran Pinjaman A.n Adi Purwanto',416667.00,17500.00,1,'2023-07-03 18:03:01','2023-07-03 18:03:01'),
	(13,4,'012307030007','2023-07-03','0015000003','Angsuran Pinjaman A.n Adi Purwanto',416667.00,17500.00,1,'2023-07-03 18:03:23','2023-07-03 18:03:23'),
	(14,4,'012307030008','2023-07-03','0015000003','Angsuran Pinjaman A.n Adi Purwanto',416667.00,17500.00,1,'2023-07-03 18:03:33','2023-07-03 18:03:33'),
	(15,4,'012307030009','2023-07-03','0015000003','Angsuran Pinjaman A.n Adi Purwanto',416667.00,17500.00,1,'2023-07-03 18:03:44','2023-07-03 18:03:44'),
	(16,4,'012307030010','2023-07-03','0015000003','Angsuran Pinjaman A.n Adi Purwanto',416667.00,17500.00,1,'2023-07-03 18:03:50','2023-07-03 18:03:50'),
	(17,4,'012307030011','2023-07-03','0015000003','Angsuran Pinjaman A.n Adi Purwanto',400000.00,17500.00,1,'2023-07-03 18:04:01','2023-07-03 18:04:01'),
	(18,4,'012307030012','2023-07-03','0015000003','Angsuran Pinjaman A.n Adi Purwanto',416663.00,17500.00,1,'2023-07-03 18:05:01','2023-07-03 18:05:01');

/*!40000 ALTER TABLE `pby_mutasi` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pby_pengajuan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pby_pengajuan`;

CREATE TABLE `pby_pengajuan` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `id_anggota` bigint(11) NOT NULL,
  `id_order` bigint(11) NOT NULL,
  `id_pinjaman` bigint(11) NOT NULL,
  `no_rek` varchar(20) NOT NULL,
  `no_pengajuan` varchar(20) NOT NULL DEFAULT '',
  `tanggal` date NOT NULL,
  `jenis` enum('Pinjaman Tunai','Cicilan Barang') NOT NULL DEFAULT 'Pinjaman Tunai',
  `nominal` double(15,2) NOT NULL DEFAULT 0.00,
  `jangka` int(3) NOT NULL DEFAULT 0,
  `keperluan` varchar(255) NOT NULL DEFAULT '',
  `jaminan` enum('Tanpa Jaminan','BPKB','Sertifikat','Lainnya') NOT NULL DEFAULT 'Tanpa Jaminan',
  `user_id` int(10) NOT NULL,
  `status_pengajuan` enum('Menunggu Persetujuan','Disetujui','Ditolak') NOT NULL DEFAULT 'Menunggu Persetujuan',
  `tgl_ubah` date DEFAULT NULL,
  `keterangan` varchar(255) NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `pby_pengajuan` WRITE;
/*!40000 ALTER TABLE `pby_pengajuan` DISABLE KEYS */;

INSERT INTO `pby_pengajuan` (`id`, `id_anggota`, `id_order`, `id_pinjaman`, `no_rek`, `no_pengajuan`, `tanggal`, `jenis`, `nominal`, `jangka`, `keperluan`, `jaminan`, `user_id`, `status_pengajuan`, `tgl_ubah`, `keterangan`, `created_at`, `updated_at`)
VALUES
	(2,1,0,1,'0015000001','P012305240001','2023-05-24','Pinjaman Tunai',10000000.00,12,'modal usaha','Tanpa Jaminan',18,'Disetujui','2023-05-24','','2023-05-24 15:13:51','2023-05-24 15:32:07'),
	(3,1,1,4,'0015100001','P012305240002','2023-05-24','Cicilan Barang',21000000.00,12,'Cicilan Macbook Pro M1','Tanpa Jaminan',18,'Disetujui','2023-05-24','','2023-05-24 15:40:02','2023-05-24 15:54:31'),
	(4,2,0,1,'0015000002','P012306080001','2023-06-08','Pinjaman Tunai',12000000.00,12,'modal','Tanpa Jaminan',30,'Disetujui','2023-06-08','','2023-06-08 15:20:26','2023-06-08 15:26:56'),
	(5,1,0,1,'0015000003','P012306120001','2023-06-12','Pinjaman Tunai',5000000.00,12,'biaya berobay','Tanpa Jaminan',1,'Disetujui','2023-06-13','','2023-06-12 08:38:56','2023-06-13 16:33:55'),
	(6,1,0,1,'','P012307010001','2023-07-01','Pinjaman Tunai',5000000.00,12,'UpacarA','Tanpa Jaminan',1,'Disetujui','2023-07-01','','2023-07-01 18:56:25','2023-07-01 18:57:21'),
	(7,1,0,1,'','P012307230001','2023-07-23','Pinjaman Tunai',50000000.00,24,'Modal usaha','Tanpa Jaminan',18,'Menunggu Persetujuan',NULL,'','2023-07-23 09:00:57','2023-07-23 09:00:57'),
	(8,1,0,1,'','P012307270001','2023-07-27','Pinjaman Tunai',300000.00,12,'Modal','Tanpa Jaminan',18,'Menunggu Persetujuan',NULL,'','2023-07-27 18:06:37','2023-07-27 18:06:37');

/*!40000 ALTER TABLE `pby_pengajuan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pby_rekening
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pby_rekening`;

CREATE TABLE `pby_rekening` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `id_anggota` bigint(11) NOT NULL DEFAULT 0,
  `id_pinjaman` bigint(11) NOT NULL DEFAULT 0,
  `id_pengajuan` bigint(11) NOT NULL DEFAULT 0,
  `no_rek` varchar(20) NOT NULL,
  `tgl_cair` date DEFAULT NULL,
  `jangka` int(3) NOT NULL DEFAULT 0,
  `jth_tempo` date DEFAULT NULL,
  `plafond` double(15,2) NOT NULL DEFAULT 0.00,
  `bya_adm` double(15,2) NOT NULL DEFAULT 0.00,
  `angske` int(3) NOT NULL DEFAULT 0,
  `saldo_awal_pokok_sys` double(15,2) NOT NULL DEFAULT 0.00,
  `saldo_awal_jasa_sys` double(15,2) NOT NULL DEFAULT 0.00,
  `saldo_akhir` double(15,2) NOT NULL DEFAULT 0.00,
  `status` enum('Aktif','Lunas') NOT NULL DEFAULT 'Aktif',
  `user_id` int(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `pby_rekening` WRITE;
/*!40000 ALTER TABLE `pby_rekening` DISABLE KEYS */;

INSERT INTO `pby_rekening` (`id`, `id_anggota`, `id_pinjaman`, `id_pengajuan`, `no_rek`, `tgl_cair`, `jangka`, `jth_tempo`, `plafond`, `bya_adm`, `angske`, `saldo_awal_pokok_sys`, `saldo_awal_jasa_sys`, `saldo_akhir`, `status`, `user_id`, `created_at`, `updated_at`)
VALUES
	(1,1,1,2,'0015000001','2023-05-24',12,'2024-05-24',10000000.00,50000.00,5,0.00,0.00,7033334.00,'Aktif',1,'2023-05-24 15:32:07','2023-06-12 08:39:38'),
	(2,1,4,3,'0015100001','2023-05-24',12,'2024-05-24',21000000.00,0.00,2,0.00,0.00,19250000.00,'Aktif',1,'2023-05-24 15:54:31','2023-06-08 14:33:59'),
	(3,2,1,4,'0015000002','2023-06-08',12,'2024-06-08',12000000.00,60000.00,2,0.00,0.00,11000000.00,'Aktif',1,'2023-06-08 15:26:56','2023-06-08 15:29:31'),
	(4,1,1,5,'0015000003','2023-06-13',12,'2024-06-13',5000000.00,25000.00,13,0.00,0.00,50001.00,'Aktif',1,'2023-06-13 16:33:55','2023-07-03 18:05:01');

/*!40000 ALTER TABLE `pby_rekening` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pby_simulasi
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pby_simulasi`;

CREATE TABLE `pby_simulasi` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `angske` int(4) NOT NULL DEFAULT 0,
  `angs_pokok` double(15,2) NOT NULL DEFAULT 0.00,
  `angs_jasa` double(15,2) NOT NULL DEFAULT 0.00,
  `user_id` int(10) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `pby_simulasi` WRITE;
/*!40000 ALTER TABLE `pby_simulasi` DISABLE KEYS */;

INSERT INTO `pby_simulasi` (`id`, `angske`, `angs_pokok`, `angs_jasa`, `user_id`, `created_at`, `updated_at`)
VALUES
	(1,1,5000000.00,0.00,30,'2023-06-08 15:26:32','2023-06-08 15:26:32'),
	(2,2,5000000.00,0.00,30,'2023-06-08 15:26:32','2023-06-08 15:26:32'),
	(3,3,5000000.00,0.00,30,'2023-06-08 15:26:32','2023-06-08 15:26:32');

/*!40000 ALTER TABLE `pby_simulasi` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table shu_jasaagt
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shu_jasaagt`;

CREATE TABLE `shu_jasaagt` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `id_anggota` bigint(11) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `no_trx` varchar(20) NOT NULL DEFAULT '',
  `hpp` double(15,2) NOT NULL DEFAULT 0.00,
  `harga_jual` double(15,2) NOT NULL DEFAULT 0.00,
  `nominal` double(15,2) NOT NULL DEFAULT 0.00,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `shu_jasaagt` WRITE;
/*!40000 ALTER TABLE `shu_jasaagt` DISABLE KEYS */;

INSERT INTO `shu_jasaagt` (`id`, `id_anggota`, `tanggal`, `no_trx`, `hpp`, `harga_jual`, `nominal`, `created_at`, `updated_at`)
VALUES
	(1,1,'2023-06-07','012306070001',0.00,0.00,35000.00,'2023-06-07 10:28:12','2023-06-07 10:28:12'),
	(2,1,'2023-06-08','012306080001',0.00,0.00,0.00,'2023-06-08 14:33:59','2023-06-08 14:33:59'),
	(3,2,'2023-06-08','012306080005',0.00,0.00,42000.00,'2023-06-08 15:29:31','2023-06-08 15:29:31'),
	(4,1,'2023-06-09','012306090001',0.00,0.00,35000.00,'2023-06-09 08:42:17','2023-06-09 08:42:17'),
	(5,1,'2023-06-10','012306100001',0.00,0.00,35000.00,'2023-06-10 08:34:51','2023-06-10 08:34:51'),
	(6,1,'2023-06-12','012306120002',0.00,0.00,35000.00,'2023-06-12 08:39:38','2023-06-12 08:39:38'),
	(7,1,'2023-07-03','012307030001',0.00,0.00,17500.00,'2023-07-03 18:00:25','2023-07-03 18:00:25'),
	(8,1,'2023-07-03','012307030002',0.00,0.00,17500.00,'2023-07-03 18:01:54','2023-07-03 18:01:54'),
	(9,1,'2023-07-03','012307030003',0.00,0.00,17500.00,'2023-07-03 18:02:14','2023-07-03 18:02:14'),
	(10,1,'2023-07-03','012307030004',0.00,0.00,17500.00,'2023-07-03 18:02:31','2023-07-03 18:02:31'),
	(11,1,'2023-07-03','012307030005',0.00,0.00,17500.00,'2023-07-03 18:02:50','2023-07-03 18:02:50'),
	(12,1,'2023-07-03','012307030006',0.00,0.00,17500.00,'2023-07-03 18:03:01','2023-07-03 18:03:01'),
	(13,1,'2023-07-03','012307030007',0.00,0.00,17500.00,'2023-07-03 18:03:23','2023-07-03 18:03:23'),
	(14,1,'2023-07-03','012307030008',0.00,0.00,17500.00,'2023-07-03 18:03:33','2023-07-03 18:03:33'),
	(15,1,'2023-07-03','012307030009',0.00,0.00,17500.00,'2023-07-03 18:03:44','2023-07-03 18:03:44'),
	(16,1,'2023-07-03','012307030010',0.00,0.00,17500.00,'2023-07-03 18:03:50','2023-07-03 18:03:50'),
	(17,1,'2023-07-03','012307030011',0.00,0.00,17500.00,'2023-07-03 18:04:01','2023-07-03 18:04:01'),
	(18,1,'2023-07-03','012307030012',0.00,0.00,17500.00,'2023-07-03 18:05:01','2023-07-03 18:05:01');

/*!40000 ALTER TABLE `shu_jasaagt` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table simp_import
# ------------------------------------------------------------

DROP TABLE IF EXISTS `simp_import`;

CREATE TABLE `simp_import` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `id_norek` bigint(11) NOT NULL,
  `no_rek` varchar(25) NOT NULL DEFAULT '',
  `nama_anggota` varchar(255) NOT NULL DEFAULT '',
  `nama_simpanan` varchar(100) NOT NULL DEFAULT '',
  `nominal` double(15,2) NOT NULL DEFAULT 0.00,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table simp_master
# ------------------------------------------------------------

DROP TABLE IF EXISTS `simp_master`;

CREATE TABLE `simp_master` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `kode` varchar(2) NOT NULL DEFAULT '',
  `nama` varchar(100) NOT NULL DEFAULT '',
  `akun_produk` varchar(20) NOT NULL DEFAULT '',
  `akun_jasa` varchar(20) NOT NULL DEFAULT '',
  `persen_jasa` double(15,2) NOT NULL DEFAULT 0.00,
  `modal` varchar(1) NOT NULL DEFAULT 'N',
  `status` varchar(1) NOT NULL DEFAULT 'Y',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `simp_master` WRITE;
/*!40000 ALTER TABLE `simp_master` DISABLE KEYS */;

INSERT INTO `simp_master` (`id`, `kode`, `nama`, `akun_produk`, `akun_jasa`, `persen_jasa`, `modal`, `status`, `created_at`, `updated_at`)
VALUES
	(1,'01','Simpanan Pokok','00120201','',0.00,'Y','Y','2023-05-04 12:50:28','2023-05-04 12:50:28'),
	(2,'02','Simpanan Wajib','00120202','',0.00,'Y','Y','2023-05-04 12:50:34','2023-05-04 12:50:34'),
	(3,'03','Simpanan Sukarela','00120203','',0.00,'N','Y','2023-05-04 12:50:47','2023-05-04 12:50:47');

/*!40000 ALTER TABLE `simp_master` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table simp_mutasi
# ------------------------------------------------------------

DROP TABLE IF EXISTS `simp_mutasi`;

CREATE TABLE `simp_mutasi` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `id_norek` bigint(11) NOT NULL,
  `no_bukti` varchar(30) NOT NULL DEFAULT '',
  `tanggal` date DEFAULT NULL,
  `no_rek` varchar(20) NOT NULL DEFAULT '',
  `keterangan` varchar(255) NOT NULL DEFAULT '',
  `debet` double(15,2) NOT NULL DEFAULT 0.00,
  `kredit` double(15,2) NOT NULL DEFAULT 0.00,
  `user_id` int(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `simp_mutasi` WRITE;
/*!40000 ALTER TABLE `simp_mutasi` DISABLE KEYS */;

INSERT INTO `simp_mutasi` (`id`, `id_norek`, `no_bukti`, `tanggal`, `no_rek`, `keterangan`, `debet`, `kredit`, `user_id`, `created_at`, `updated_at`)
VALUES
	(2,2,'012305240002','2023-05-24','0010200001','Setoran Simpanan Wajib A.n Adi Purwanto',0.00,150000.00,1,'2023-05-24 13:34:22','2023-05-24 13:34:22'),
	(3,1,'012305240003','2023-05-24','0010100001','Setoran Simpanan Pokok A.n Adi Purwanto',0.00,100000.00,1,'2023-05-24 15:08:11','2023-05-24 15:08:11'),
	(4,3,'012306080002','2023-06-08','0010100002','Setoran Simpanan Pokok A.n Pandega',0.00,100000.00,1,'2023-06-08 15:12:02','2023-06-08 15:12:02'),
	(5,4,'012306080003','2023-06-08','0010200002','Setoran Simpanan Wajib A.n Pandega',0.00,150000.00,1,'2023-06-08 15:12:12','2023-06-08 15:12:12'),
	(6,1,'012306120001','2023-06-12','0010100001','simpanan pokok a.n adi purwanto',0.00,300000.00,1,'2023-06-12 08:38:01','2023-06-12 08:38:01');

/*!40000 ALTER TABLE `simp_mutasi` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table simp_rekening
# ------------------------------------------------------------

DROP TABLE IF EXISTS `simp_rekening`;

CREATE TABLE `simp_rekening` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `id_anggota` bigint(11) NOT NULL,
  `id_simpanan` bigint(11) NOT NULL,
  `no_rek` varchar(15) NOT NULL DEFAULT '',
  `tgl_buka` date DEFAULT NULL,
  `tgl_tutup` date DEFAULT NULL,
  `jasa_persen` double(5,2) NOT NULL DEFAULT 0.00,
  `status_aktif` varchar(1) NOT NULL DEFAULT 'Y',
  `status_blokir` varchar(1) NOT NULL DEFAULT 'N',
  `saldo_awal_sys` double(15,2) NOT NULL DEFAULT 0.00,
  `saldo_akhir` double(15,2) NOT NULL DEFAULT 0.00,
  `user_id` int(10) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `simp_rekening` WRITE;
/*!40000 ALTER TABLE `simp_rekening` DISABLE KEYS */;

INSERT INTO `simp_rekening` (`id`, `id_anggota`, `id_simpanan`, `no_rek`, `tgl_buka`, `tgl_tutup`, `jasa_persen`, `status_aktif`, `status_blokir`, `saldo_awal_sys`, `saldo_akhir`, `user_id`, `created_at`, `updated_at`)
VALUES
	(1,1,1,'0010100001','2023-05-24',NULL,0.00,'Y','N',0.00,400000.00,18,'2023-05-24 13:17:33','2023-05-24 15:07:42'),
	(2,1,2,'0010200001','2023-05-24',NULL,0.00,'Y','N',0.00,150000.00,18,'2023-05-24 13:17:33','2023-05-24 13:17:33'),
	(3,2,1,'0010100002','2023-06-08',NULL,0.00,'Y','N',0.00,100000.00,30,'2023-06-08 15:10:16','2023-06-08 15:10:16'),
	(4,2,2,'0010200002','2023-06-08',NULL,0.00,'Y','N',0.00,150000.00,30,'2023-06-08 15:10:16','2023-06-08 15:10:16'),
	(5,1,1,'0010100003','2023-08-22',NULL,0.00,'Y','N',0.00,0.00,1,'2023-08-22 13:06:16','2023-08-22 13:06:16');

/*!40000 ALTER TABLE `simp_rekening` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sys_pengesah
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sys_pengesah`;

CREATE TABLE `sys_pengesah` (
  `ketua` varchar(30) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `sekretaris` varchar(30) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `bendahara` varchar(30) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `manager` varchar(30) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `ko` varchar(30) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `keuangan` varchar(30) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `marketing` varchar(30) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `kasir` varchar(30) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `kabag_pby` varchar(30) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `saksi1` varchar(30) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `saksi2` varchar(30) COLLATE latin1_general_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

LOCK TABLES `sys_pengesah` WRITE;
/*!40000 ALTER TABLE `sys_pengesah` DISABLE KEYS */;

INSERT INTO `sys_pengesah` (`ketua`, `sekretaris`, `bendahara`, `manager`, `ko`, `keuangan`, `marketing`, `kasir`, `kabag_pby`, `saksi1`, `saksi2`)
VALUES
	('Ketua','Sekretaris','Bendahara','Manager','','Akunting','','Kasir','Ka. Pembiayaan','Saksi 1','Saksi 2');

/*!40000 ALTER TABLE `sys_pengesah` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sys_periode
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sys_periode`;

CREATE TABLE `sys_periode` (
  `kde_kantor` char(3) COLLATE latin1_general_ci DEFAULT NULL,
  `tgl_aktif` date DEFAULT NULL,
  `tgl_hariini` date DEFAULT NULL,
  `bagi_hasil_reguler` double DEFAULT NULL,
  `bagi_hasil_berjangka` double DEFAULT NULL,
  `openclose` varchar(5) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `posting_penyusutan` double NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

LOCK TABLES `sys_periode` WRITE;
/*!40000 ALTER TABLE `sys_periode` DISABLE KEYS */;

INSERT INTO `sys_periode` (`kde_kantor`, `tgl_aktif`, `tgl_hariini`, `bagi_hasil_reguler`, `bagi_hasil_berjangka`, `openclose`, `posting_penyusutan`)
VALUES
	('001','2020-01-01','2020-03-31',1,0,'Buka',0);

/*!40000 ALTER TABLE `sys_periode` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sys_perush
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sys_perush`;

CREATE TABLE `sys_perush` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kde_wilayah` varchar(4) COLLATE latin1_general_ci DEFAULT NULL COMMENT 'id order',
  `kde_kantor` char(3) COLLATE latin1_general_ci DEFAULT NULL,
  `nma_perush` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `nma_cabang` varchar(30) COLLATE latin1_general_ci DEFAULT '0',
  `alm_perush` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `kta_perush` varchar(25) COLLATE latin1_general_ci DEFAULT '',
  `tlp_perush` varchar(20) COLLATE latin1_general_ci DEFAULT '',
  `eml_perush` varchar(35) COLLATE latin1_general_ci DEFAULT '',
  `website` varchar(70) COLLATE latin1_general_ci DEFAULT NULL,
  `paket` double DEFAULT 1,
  `sn` varchar(30) COLLATE latin1_general_ci DEFAULT '',
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

LOCK TABLES `sys_perush` WRITE;
/*!40000 ALTER TABLE `sys_perush` DISABLE KEYS */;

INSERT INTO `sys_perush` (`id`, `kde_wilayah`, `kde_kantor`, `nma_perush`, `nma_cabang`, `alm_perush`, `kta_perush`, `tlp_perush`, `eml_perush`, `website`, `paket`, `sn`, `updated_at`, `created_at`)
VALUES
	(1,'3326','001','KOPERASI PARAGON UNTUNG BARENG','JAKARTA','Jl. Falatehan I No. 38','Jakarta','','admin@koperasiuntungbareng.com','',1,'','2023-04-10 15:53:39','0000-00-00 00:00:00');

/*!40000 ALTER TABLE `sys_perush` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sys_rekapshu
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sys_rekapshu`;

CREATE TABLE `sys_rekapshu` (
  `tahun` double NOT NULL DEFAULT 0,
  `no` double NOT NULL DEFAULT 0,
  `poin` varchar(30) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `persen` double(5,2) NOT NULL DEFAULT 0.00,
  `perolehan` double(15,2) NOT NULL DEFAULT 0.00
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;



# Dump of table sys_setshu
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sys_setshu`;

CREATE TABLE `sys_setshu` (
  `no` double NOT NULL,
  `nama` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `persen` double(5,2) NOT NULL,
  `akun` varchar(14) COLLATE latin1_general_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

LOCK TABLES `sys_setshu` WRITE;
/*!40000 ALTER TABLE `sys_setshu` DISABLE KEYS */;

INSERT INTO `sys_setshu` (`no`, `nama`, `persen`, `akun`)
VALUES
	(1,'Jasa Simpanan',20.00,''),
	(2,'Jasa Pembiayaan',25.00,''),
	(3,'Dana Cadangan',55.00,'001203');

/*!40000 ALTER TABLE `sys_setshu` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sys_settfas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sys_settfas`;

CREATE TABLE `sys_settfas` (
  `no_reklama` double DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

LOCK TABLES `sys_settfas` WRITE;
/*!40000 ALTER TABLE `sys_settfas` DISABLE KEYS */;

INSERT INTO `sys_settfas` (`no_reklama`)
VALUES
	(1);

/*!40000 ALTER TABLE `sys_settfas` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tabungan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tabungan`;

CREATE TABLE `tabungan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `anggota_id` int(10) unsigned NOT NULL,
  `saldo` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tabungan_anggota_id_foreign` (`anggota_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` enum('Admin','Anggota') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Anggota',
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `name`, `email`, `password`, `role`, `phone`, `remember_token`, `created_at`, `updated_at`)
VALUES
	(1,'Administrator','admin@gmail.com','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Admin','','PiRbnQ3N1KL51QpNHaOK7qrruo5kuHSAKjWwcQa7c0IzsNc2XqQuMKZBJSuj','2023-03-27 11:57:50','2023-03-27 11:57:50'),
	(18,'Adi Purwanto','adi@pincgroup.id','$2y$10$cKICtnd5.ofm1m9hXa8uf.Ty2LrP2wsyAbKx6bVq4MIzjwPLVg0r2','Anggota',NULL,'iBkKHOBOMyyM8TjvuY2Hn4S3ncqyBJ8KigwLTa2To70oh57PesFtSh30GaGG','2023-05-24 13:17:18','2023-05-24 13:17:18'),
	(19,'Admin 2','admin2@gmail.com','$2y$10$bfpx68T9DRygPfpfVeDMb./nJbO0CIwVPYjOZZ..9DF6ArW7.4FI2','Admin',NULL,NULL,'2023-05-29 15:32:39','2023-05-29 15:32:39'),
	(30,'Pandega','pandega@pantarei.id','$2y$10$ENwD6miblNeRQgdJLu..xuYZf6o2Sb9sipWCVEFqsf0q/v.4taLJO','Anggota',NULL,NULL,'2023-06-08 15:09:38','2023-06-08 15:09:38'),
	(21,'Rani Indriani','rani@pincgroup.id','$2y$10$F/po29jCMqBfKmlTXwU/2uHRpQ7m0M5C4XhydPvmoUzkU4Pg/tMFq','Admin',NULL,NULL,'2023-05-29 16:26:20','2023-05-29 16:26:20'),
	(22,'Gita Galantari','gita@pincgroup.id','$2y$10$6RvUYolx.cv6GKOMzl.MF.DTOtzKNiHvkaFONvAVP4t3dprfEgc/.','Admin',NULL,NULL,'2023-05-29 16:27:42','2023-05-29 16:27:42'),
	(23,'Dewi Kemalasari','mala@pincgroup.id','$2y$10$9zmYtUVBwMxFQj3Y5j5wAun1iacoAeC2.HsDqSbebhk3BER.4tSf6','Admin',NULL,NULL,'2023-05-29 16:31:47','2023-05-29 16:31:47'),
	(24,'Tri Muttaqin','tri.muttaqin@pincgroup.id','$2y$10$2G6yKhV.7jDlKzXQcYPHtOujyfcfMLGGxLwddCw716HNTgKMGj/mS','Admin',NULL,NULL,'2023-05-29 16:41:34','2023-05-29 16:41:34'),
	(25,'M. Feariansyah','m.feariansyah@mataangin.agency','$2y$10$XZCknGzD1reaouKfBFxTQuUserRo/PlOT2jvyJ6V6G2Z7NxU0APES','Admin',NULL,NULL,'2023-05-29 16:42:06','2023-05-29 16:42:06'),
	(26,'Cindy Marcela','cindy.marcela@pantarei.id','$2y$10$jkiBKQWDzrin/p9e3c2vjOaMlbEL1qjW/fB7AGhAk/Az95Ivuz62.','Admin',NULL,NULL,'2023-05-29 17:42:54','2023-05-29 17:42:54'),
	(27,'Pandega','pandega@pincgroup.id','$2y$10$WzUYL0neOPrfyydNEvGfN.Dr9wmw0guRFtuGleuBb.Co/Nw.uhYA6','Admin',NULL,NULL,'2023-05-29 17:43:52','2023-05-29 17:43:52'),
	(28,'Regina','regina@mataangin.agency','$2y$10$1x1DZv7mV7JZd3EAHNNfHO4CStOuuBoN4OrXLl5SEgGUEv/GwTxNS','Admin',NULL,NULL,'2023-05-29 17:45:05','2023-05-29 17:45:05');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
