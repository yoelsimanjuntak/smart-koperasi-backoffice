@extends('layouts.app')
@section('content-app')
<div class="row row-cards row-deck">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Detail Pengajuan Pinjaman</h3>
                <div class="card-options">
                    <blockquote class="blockquote">
                        <h4>{{ $JbOrder->Anggota->nama_anggota }}</h4>
                        <h5>{{ $JbOrder->Anggota->Department->nama }}  -  {{ $JbOrder->Anggota->Perusahaan->nama }}</h5>
                        <h4>{{ $JbOrder->Anggota->status_karyawan }}</h4>
                    </blockquote>  
                </div>
            </div>
            <div class="card-body">
              <h3>Detail Pesanan</h3>
              <div class="row">
                <div class="col-md-3">
                    <div class="card-people mt-auto">
                        <img src="{{ asset('public/images') }}/{{ $JbOrder->MsProduk->foto }}" alt="people">
                    </div>
                </div>
                <div class="col-md-9">
                    <table id="#" class="table table-striped table-hover" width="100%" cellspacing="0">
                        <tbody>
                            <tr>
                                <td width="20%">Nama Barang</td>
                                <td width="5%">:</td>
                                <td>{{  $JbOrder->MsProduk->nama_barang }}<td>
                            </tr>                            
                            <tr>
                                <td width="20%">Deskripsi</td>
                                <td width="5%">:</td>
                                <td><blockquote>
                                    {{ $JbOrder->MsProduk->deskripsi }}</blockquote><td>
                            </tr>
                            <tr>
                              <td width="20%">No. Pesanan</td>
                              <td>:</td>
                              <td>{{  $JbOrder->no_trx }}<td>
                          </tr>
                          <tr>
                              <td width="20%">Tanggal</td>
                              <td width="5%">:</td>
                              <td>{{  date('d-m-Y', strtotime($JbOrder->tanggal)) }}<td>
                          </tr>
                           
                        </tbody>
                    </table>
                </div> 
              </div> 
                <table id="#" class="table table-striped table-hover" width="100%" cellspacing="0">
                    <tbody>                        
                        <tr>
                            <td width="20%">Harga Barang</td>
                            <td width="5%">:</td>
                            <td> Rp. {{  number_format($JbOrder->harga,2) }}<td>
                        </tr>
                        <tr>
                          <td width="20%">Jumlah Barang</td>
                          <td width="5%">:</td>
                          <td>{{  $JbOrder->qty }}<td>
                      </tr>
                        <tr>
                            <td width="20%">Skema Pembayaran</td>
                            <td width="5%">:</td>
                            <td>{{  $JbOrder->pembayaran}} {{ $JbOrder->pembayaran=="Cicilan" ? '12x' : '' }}
                              @if ($JbOrder->pembayaran=="Cicilan")
                                <p>
                                  <small class="text-muted">
                                      Rp. {{ number_format(($JbOrder->harga/12),2) }} / bulan
                                  </small>
                                </p>  
                              @endif
                              
                            <td>
                        </tr>
                        <tr>
                            <td width="20%">Status Pengajuan</td>
                            <td width="5%">:</td>
                            <td>{{ $JbOrder->status_order }}<td>
                        </tr>
                    </tbody>
                </table>
                <br>
                @switch($JbOrder->status_order)
                  @case('Menunggu Konfirmasi')
                      <div class="row">
                        <div class="col-md-12">
                          <div class="pull-right">
                              <a href="javascript: history.go(-1)" class="btn btn-danger btn-sm"><i class="fa fa-reply"></i>  Batal</a>

                              <a href="#modaltolak" onclick="$('#idtolak').val({{$JbOrder->id}})" data-toggle="modal" data-placement="top" class="btn btn-warning btn-sm" title="Batalkan Pesanan"><i class="fa fa-fw fa-close"></i> Batalkan Pesanan</a>

                              <a href="#modalsetuju" onclick="$('#idsetuju').val({{$JbOrder->id}})" data-toggle="modal" data-placement="top" class="btn btn-success btn-sm" title="Proses Pesanan"><i class="fa fa-fw fa-check"></i> Proses Pesanan</a>

                            </div>
                        </div>
                      </div>
                    @break
                  @case('Diproses')
                    <div class="row">
                      <div class="col-md-12">
                          <div class="pull-right">
                              <a href="javascript: history.go(-1)" class="btn btn-danger btn-sm"><i class="fa fa-reply"></i>  Batal</a>

                              <a href="#modalsiap" onclick="$('#idsiap').val({{$JbOrder->id}})" data-toggle="modal" data-placement="top" class="btn btn-info btn-sm" title="Siap Diambil"><i class="fa fa-fw fa-info-circle"></i> Siap Diambil</a>
                          </div>
                      </div>
                    </div>
                  @break
                  @case('Siap Diambil')
                    <div class="row">
                      <div class="col-md-12">
                          <div class="pull-right">
                              <a href="javascript: history.go(-1)" class="btn btn-danger btn-sm"><i class="fa fa-reply"></i>  Batal</a>

                              <a href="#modaldone" onclick="$('#iddone').val({{$JbOrder->id}})" data-toggle="modal" data-placement="top" class="btn btn-success btn-sm" title="Pesanan Selesai"><i class="fa fa-fw fa-check"></i> Pesanan Selesai</a>
                          </div>
                      </div>
                    </div>
                  @break
                
                  @default
                    
                
                
                  
                @endswitch
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modaltolak" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
      <form action="{{url('admin/purchasing/update')}}" method="POST">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="myModalLabel">Batalkan Pesanan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label>Alasan Pembatalan</label>
            <input type="text" name="ket_batal"class="form-control" >
            <span class="help-block">{{$errors->first('ket_batal')}}</span>
          </div>
        </div>
        <div class="modal-footer">
            <input type="hidden" id="idtolak" name="id" value="">
            <input type="hidden" name="sts_pengajuan" value="Dibatalkan">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            <input type="submit" class="btn btn-danger" value="Batalkan Pesanan">
          </form>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
  </form>
    <!-- /.modal-dialog -->
  </div>

  <div class="modal fade" id="modalsetuju" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="myModalLabel">Proses Pesanan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          Apakah pesanan tersebut akan diproses?
        </div>
        <div class="modal-footer">
          <form action="{{url('admin/purchasing/update')}}" method="POST">
            <input type="hidden" id="idsetuju" name="id" value="">
            <input type="hidden" name="sts_pengajuan" value="Diproses">
            <input type="hidden" name="ket_batal" value="" >
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            <input type="submit" class="btn btn-success" value="Proses Pesanan">
          </form>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>

  <div class="modal fade" id="modalsiap" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="myModalLabel">Selesaikan Pesanan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          Apakah pesanan tersebut akan diselesaikan?
        </div>
        <div class="modal-footer">
          <form action="{{url('admin/purchasing/update')}}" method="POST">
            <input type="hidden" id="idsiap" name="id" value="">
            <input type="hidden" name="sts_pengajuan" value="Siap Diambil">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            <input type="submit" class="btn btn-success" value="Proses Pesanan">
          </form>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>

  <div class="modal fade" id="modaldone" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="myModalLabel">Selesaikan Pesanan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          Apakah pesanan tersebut akan diselesaikan?
        </div>
        <div class="modal-footer">
          <form action="{{url('admin/purchasing/update')}}" method="POST">
            <input type="hidden" id="iddone" name="id" value="">
            <input type="hidden" name="sts_pengajuan" value="Selesai">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            <input type="submit" class="btn btn-success" value="Selesaikan Pesanan">
          </form>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
@endsection
