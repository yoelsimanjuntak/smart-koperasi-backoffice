@extends('layouts.app')
@section('content-app')
@include('message.flash')
<div class="row row-cards row-deck">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Informasi Anggota</h3>                
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="card-people mt-auto">
                            <img src="{{ asset('public/images') }}/{{ $MsAnggota->foto_ktp }}" alt="people">
                        </div>
                    </div>
                    <div class="col-md-9">
                        <table id="#" class="table table-striped table-hover" width="100%" cellspacing="0">
                            <tbody>
                                <tr>
                                    <td width="20%">NIK (ID Karyawan)</td>
                                    <td width="5%">:</td>
                                    <td align="left">{{  $MsAnggota->nik }}<td>
                                </tr>
                                <tr>
                                    <td width="20%">Nama Anggota</td>
                                    <td width="5%">:</td>
                                    <td align="left">{{  $MsAnggota->nama_anggota }}<td>
                                </tr>
                                <tr>
                                    <td width="20%">Perusahaan</td>
                                    <td>:</td>
                                    <td>{{  $MsAnggota->Perusahaan->nama }} - {{  $MsAnggota->Department->nama }}<td>
                                </tr>
                                <tr>
                                    <td width="20%">Status Karyawan</td>
                                    <td>:</td>
                                    <td>{{  $MsAnggota->status_karyawan }}<td>
                                </tr>
                                <tr>
                                    <td width="20%">Jenis Kelamin</td>
                                    <td>:</td>
                                    <td>{{  $MsAnggota->jenis_kelamin }}<td>
                                </tr>
                                <tr>
                                    <td width="20%">Tempat, Tanggal Lahir</td>
                                    <td>:</td>
                                    <td>{{  $MsAnggota->tempat_lahir }}, {{ \Carbon\Carbon::parse($MsAnggota->tgl_lahir)->format('d F Y') }}<td>
                                </tr>
                                <tr>
                                    <td width="20%">Email</td>
                                    <td>:</td>
                                    <td>{{  $MsAnggota->email }}<td>
                                </tr>
                                <tr>
                                    <td width="20%">No. Telp</td>
                                    <td>:</td>
                                    <td>{{  $MsAnggota->no_telpon }}<td>
                                </tr>
                                <tr>
                                    <td width="20%">Alamat</td>
                                    <td>:</td>
                                    <td>{{  $MsAnggota->alamat }}<td>
                                </tr>
                                <tr>
                                    <td width="20%">Alamat Domisili</td>
                                    <td>:</td>
                                    <td>{{  $MsAnggota->alamat_domisili }}<td>
                                </tr>
                            <tbody>
                        </table>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="pull-right">
                                    <a href="javascript: history.go(-1)" class="btn btn-danger btn-sm"><i class="fa fa-reply"></i>  Batal</a>
                                  </div>
                              </div>
                          </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
