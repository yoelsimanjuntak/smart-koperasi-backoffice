<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cetak Formulir Pendaftaran Anggota</title>
    <link rel="shortcut icon" href="{{ url('') }}/public/logokop.png" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
    <div class="page">
        <div class="page-single">
            <div class="container">
                        <div class="text-center my-5">
                            <a href="{{ url('/admin/master_anggota/download/'.$MsAnggota->id) }}"><img src="{{ asset('public/admin/template') }}/images/logokop.png"
                             alt="logo" width="300"></a>
                        </div>
                        <h6 align="center">FORMULIR PERMOHONAN KEANGGOTAAAN</h6>
                        <h6 align="center" style="text-transform: uppercase">{{ config('app.name') }}</h6>
                        <br>
                        <p>
                            Saya yang bertanda tangan di bawah ini:
                        </p>
                        <table id="#" class="table table-striped" width="100%" cellspacing="0">
                            <tbody>
                                <tr>
                                    <td width="20%">Nama Lengkap</td>
                                    <td width="5%">:</td>
                                    <td align="left">{{  $MsAnggota->nama_anggota }}<td>
                                </tr>
                            </tbody>
                        </table>
            </div>
        </div>
    </div>
</body>
</html>
