@extends('layouts.app')
@section('content-app')
@include('message.flash')
<div class="row row-cards row-deck">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Laporan Laba Rugi</h3>
                <div class="card-options">
                    <form action="{{url('admin/lap_akunting/labarugi')}}" method="POST">
                        <div class="row">
                          <div class="col-md-4">
                            <div class="form-group">
                              <label>Filter Periode</label>
                              <input type="date" name="tgl_mulai" class="form-control" value="<?php echo date('Y-m-d'); ?>" >
                            </div>                          
                          </div>
                          <div class="col-md-4">
                              <div class="form-group">
                                <label>&nbsp;</label>
                                <input type="date" name="tgl_selesai" class="form-control" value="<?php echo date('Y-m-d'); ?>">
                              </div>                          
                            </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <input type="hidden" name="_token" value="{{csrf_token()}}">
                              <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-table"></i> Tampilkan</button>
                            </div>
                          </div>
                        </div>
                      </form> 
                </div>
            </div>
            <div class="card-body">
                @if ($Pendptan <> [])
                    <table id="#" class="table table-striped table-bordered" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Kode Akun</th>
                                <th>Nama Akun</th>
                                <th>Saldo Akhir</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($Pendptan as $k)
                                <tr>
                                    <td>
                                        {{ $k->kde_akun }}
                                    </td>
                                    <td>
                                        @if ($k->pos_akun == 1)
                                            {{ $k->nma_akun }}
                                        @else
                                            &nbsp;&nbsp;{{ $k->nma_akun }}
                                        @endif
                                    </td>
                                    <td align="right">{{ number_format($k->saldo_akhir,2) }}</td>
                                </tr>                                
                            @endforeach
                            <tr>
                                <td colspan="2" align="center" class="font-weight-bold">
                                    TOTAL PENDAPATAN
                                </td>
                                <td align="right" class="font-weight-bold">
                                    {{ number_format($JmlPendpt,2) }}
                                </td>
                            </tr>
                            @foreach ($Biaya as $b)
                            <tr>
                                <td>
                                    {{ $b->kde_akun }}
                                </td>
                                <td>
                                    @if ($b->pos_akun == 1)
                                        {{ $b->nma_akun }}
                                    @else
                                        &nbsp;&nbsp;{{ $b->nma_akun }}
                                    @endif
                                </td>
                                <td align="right">{{ number_format($b->saldo_akhir,2) }}</td>
                            </tr>  
                            @endforeach
                            <tr>
                                <td colspan="2" align="center" class="font-weight-bold">
                                    TOTAL BIAYA
                                </td>
                                <td align="right" class="font-weight-bold">
                                    {{ number_format($JmlBiaya,2) }}
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center" class="font-weight-bold">
                                    SISA HASIL USAHA
                                </td>
                                <td align="right" class="font-weight-bold">
                                    {{ number_format($JmlPendpt-$JmlBiaya,2) }}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modelhapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="myModalLabel">Hapus Akun</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          Apakah akun tersebut akan dihapus ?
        </div>
        <div class="modal-footer">
          <form action="{{url('admin/master_akun/hapus')}}" method="POST">
            <input type="hidden" id="idhapus" name="id" value="">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            <input type="submit" class="btn btn-primary" value="Hapus">
          </form>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
@endsection
