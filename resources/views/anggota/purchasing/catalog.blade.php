@extends('layouts_anggota.app')
@section('content-app')
@include('message.flash')
<div class="row row-cards row-deck">
    <div class="col-12">
        <div class="#">
            <div class="#">
                <div class="card-columns">
                    @foreach ($Produk as $p)
                        <div class="card">
                            <img class="card-img-top" src="{{ asset('public/images') }}/{{ $p->foto }}" alt="">
                            <div class="card-body">
                                <h4 class="card-title mt-3">{{ $p->nama_barang }}</h4>
                                <h5 class="card-title">Rp. {{ number_format($p->harga_jual,2) }}
                                    <p>
                                        <small class="text-muted">
                                            Cicilan 12x Rp. {{ number_format(($p->harga_jual/12),2) }} / bulan
                                        </small>
                                    </p>
                                </h5>
                                <p>
                                  @switch($p->status)
                                    @case('Ready Stock')
                                      <span class="badge badge-pill badge-success">{{ $p->status }}</span>
                                      @break
                                    @case('PreOrder')
                                      <span class="badge badge-pill badge-secondary">{{ $p->status }}</span>
                                      <p>
                                        <small class="text-secondary">
                                        <i>PreOrder dalam {{ $p->estimasi }} Hari</i>
                                      </small></p>
                                      @break
                                    @case('Out of Stock')
                                      <span class="badge badge-pill badge-warning">{{ $p->status }}</span>
                                      @break
                                    @case('Discontinue')
                                      <span class="badge badge-pill badge-danger">{{ $p->status }}</span>
                                    @break
                                    @default
                                      
                                  @endswitch
                                </p>

                                <p class="card-text">{{ $p->deskripsi }}</p>
                            </div>
                            <div class="card-footer">
                              <a href="{{ url('/anggota/purchasing/addnew/'.$p->id) }}" class="btn btn-info btn-sm">Buat Pesanan</a>
                              {{--  @if ($p->cicilan=="Y")
                                <a href="{{ url('/anggota/purchasing/cicilan/'.$p->id) }}" class="btn btn-warning btn-sm">Ajukan Cicilan</a>
                              @endif

                              @if ($p->bayar_penuh=="Y")
                                <a href="{{ url('/anggota/purchasing/bayarpenuh/'.$p->id) }}" class="btn btn-info btn-sm">Bayar Penuh</a>
                              @endif  --}}
                                 
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modelhapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="myModalLabel">Hapus Akun</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          Apakah akun tersebut akan dihapus ?
        </div>
        <div class="modal-footer">
          <form action="{{url('admin/master_akun/hapus')}}" method="POST">
            <input type="hidden" id="idhapus" name="id" value="">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            <input type="submit" class="btn btn-primary" value="Hapus">
          </form>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
@endsection
