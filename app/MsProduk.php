<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MsProduk extends Model
{
    protected $table ='ms_produk';
    protected $fillable = [
        'id_kategori', 'nama_barang', 'deskripsi', 'harga_beli', 'harga_jual', 'status', 'foto', 'cicilan', 'bayar_penuh', 'estimasi'
    ];

    public function Kategori()
    {
        return $this->belongsTo('App\MsKategori', 'id_kategori');
    }

    public function JbOrder()
    {
        return $this->hasMany('App\JbOrder', 'id_produk');
    }
}
