<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MsAnggota;
use App\SimpRekening;
use App\PbyRekening;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class AgtSimpananController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $UserId     = Auth::user()->id;
        $Anggota    = MsAnggota::where('user_id', $UserId)->get();
        $IdAnggota  = [];
        $NamaAgt    = [];
        $Perush     = [];
        foreach($Anggota as $a){
            $IdAnggota = $a->id;           
            $NamaAgt    = $a->nama_anggota;
            $Perush     = $a->Perusahaan->nama;
        }

        $SimpRek    = SimpRekening::where('id_anggota', $IdAnggota)->get();
        $PbyRek     = PbyRekening::where('id_anggota', $IdAnggota)->get();

        $Simpanan = DB::select("select sum(saldo_akhir) as jml_simp from simp_rekening where status_aktif='Y' and id_anggota=?",[$IdAnggota]);
        $JmlSimp = [];
        foreach($Simpanan as $s){
            $JmlSimp = $s->jml_simp;           
        }

        return view('anggota.simpanan.index', compact('NamaAgt', 'SimpRek', 'PbyRek', 'JmlSimp', 'Perush'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $SimpRek = SimpRekening::findorfail($id);
        $IdRek  = $SimpRek->id;
        $Norek  = $SimpRek->no_rek;

        $SimpMutasi = DB::select("select * from simp_mutasi where no_rek='$Norek' order by tanggal, no_bukti ASC");
        $JmlMutasi  = DB::select("select sum(debet) as debet, sum(kredit) as kredit from simp_mutasi where no_rek='$Norek' group by no_rek");

        $JmlDebet  = [];
        $JmlKredit  = [];
        
        foreach($JmlMutasi as $j){
            $JmlDebet   = $j->debet;
            $JmlKredit   = $j->kredit;
        }   

        return view('anggota.simpanan.show', compact('SimpRek', 'SimpMutasi', 'JmlDebet', 'JmlKredit'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
