<?php

namespace App\Http\Controllers;

use App\ChartAccount;
use App\Lib\LibAkun;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LapAkuntingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function jurnal()
    {
        $KasMutasi = DB::select("SELECT t.tanggal, t.no_bukti, t.kde_akun, a.nma_akun, a.pos_akun, t.keterangan, t.debet, t.kredit, t.id FROM chart_account a, akt_mutasi t WHERE t.kde_akun=a.kde_akun order by t.id, t.no_bukti ASC, t.tanggal DESC");

        // dd($KasMutasi);
        return view('admin.lap_akunting.jurnal', compact('KasMutasi'));
    }

    public function labarugi_index()
    {
        $Labarugi   = [];
        $Pendptan = [];       
        $JmlPendpt  = [];        
        $Biaya = [];       
        $JmlBiaya  = [];
        
        return view('admin.lap_akunting.labarugi', compact('Pendptan', 'Biaya', 'JmlPendpt', 'JmlBiaya'));
    }

    public function labarugi_show(Request $req)
    {
        $TglMulai   = $req->input('tgl_mulai');
        $TglSelesai   = $req->input('tgl_selesai');
        DB::query("update chart_account set saldo_akhir =0");

        $Akun = ChartAccount::all();
        $total_akun = $Akun->count();
        foreach($Akun as $a){       
            $id         = $a->id;     
            $KodeAkun   =trim($a->kde_akun);
            $Jenis      = $a->jenis;
            if ($Jenis == 'Pendapatan' or $Jenis == 'Pasiva'){
                $GetMutasi  = DB::select("select (sum(kredit)-sum(debet)) as jumlah from akt_mutasi where kde_akun=? and tanggal>=? and tanggal<=?", [$KodeAkun, $TglMulai, $TglSelesai]);
                $JmlMutasi = [];

                foreach($GetMutasi as $j){
                    if($j->jumlah == null){
                        $JmlMutasi   = 0;
                    }else{
                        $JmlMutasi   = $j->jumlah;
                    }
                    
                    $AktAkun = ChartAccount::findorfail($id);
                    $AktAkun->update([
                        'saldo_akhir' => $JmlMutasi
                    ]);
                }
            }else{
                $GetMutasi  = DB::select("select (sum(debet)-sum(kredit)) as jumlah from akt_mutasi where kde_akun=? and tanggal>=? and tanggal<=?", [$KodeAkun, $TglMulai, $TglSelesai]);
                $JmlMutasi = [];

                foreach($GetMutasi as $j){
                    if($j->jumlah == null){
                        $JmlMutasi   = 0;
                    }else{
                        $JmlMutasi   = $j->jumlah;
                    }
                    
                    $AktAkun = ChartAccount::findorfail($id);
                    $AktAkun->update([
                        'saldo_akhir' => $JmlMutasi
                    ]);

                }
            }            
        }
        $Pendptan = ChartAccount::where('jenis', 'Pendapatan')->get();
        $TotPendpt  = DB::select("select sum(saldo_akhir) as pendpt from chart_account where jenis='Pendapatan'");
        $JmlPendpt  = [];
        foreach($TotPendpt as $p){
            $JmlPendpt  = $p->pendpt;
        }
        $Biaya = ChartAccount::where('jenis', 'Biaya')->get();
        $TotBiaya  = DB::select("select sum(saldo_akhir) as biaya from chart_account where jenis='Biaya'");
        $JmlBiaya  = [];
        foreach($TotBiaya as $b){
            $JmlBiaya  = $b->biaya;
        }

        return view('admin.lap_akunting.labarugi', compact('Pendptan', 'Biaya', 'JmlPendpt', 'JmlBiaya'));
    }

    public function bukubesar_index()
    {
        $Akun = ChartAccount::all();
        $BukBes = [];
        $KodeAkun   ='';
        $TglMulai   = Carbon::now()->format('Y-m-d');
        $TglSelesai   = Carbon::now()->format('Y-m-d');
        
        return view('admin.lap_akunting.bukubesar', compact('Akun', 'BukBes', 'KodeAkun', 'TglMulai', 'TglSelesai'));
    }

    public function bukubesar_show(Request $req)
    {
        
        $this->validate($req,[
            'kde_akun' => 'required'
        ]);
        
        $KodeAkun   = $req->input('kde_akun');
        $TglMulai   = $req->input('tgl_mulai');
        $TglSelesai   = $req->input('tgl_selesai');
        $Akun = ChartAccount::all();


        $BukBes     = DB::select("select t.tanggal, t.no_bukti, t.kde_akun, a.nma_akun, a.pos_akun, t.debet, t.kredit, t.keterangan from akt_mutasi t, chart_account a where t.kde_akun=a.kde_akun and t.kde_akun=? and t.tanggal>=? and t.tanggal<=? order by t.id", [$KodeAkun, $TglMulai, $TglSelesai]);
        // dd($BukBes);

        return view('admin.lap_akunting.bukubesar', compact('Akun', 'BukBes', 'KodeAkun', 'TglMulai', 'TglSelesai'));
    }

    public function aruskas_index(){
        $ArusKas        = [];
        $SaldoAwal      = 0;
        $Masuk          = 0;
        $Keluar         = 0;
        $SaldoAkhir     = 0;
        $TglMulai       = Carbon::now()->format('Y-m-d');
        $TglSelesai     = Carbon::now()->format('Y-m-d');
        return view('admin.lap_akunting.aruskas', compact( 'SaldoAwal', 'Masuk', 'Keluar', 'SaldoAkhir', 'TglMulai', 'TglSelesai', 'ArusKas'));
    }

    public function aruskas_show(Request $req){

        $TglMulai   = $req->input('tgl_mulai');
        $TglSelesai   = $req->input('tgl_selesai');

        // $TglAwal    = Carbon::parse($TglMulai)->subDays(1)->format('Y-m-d');

        $AkunKas    = LibAkun::AkunKAS();
        $HitungSaldo    = DB::select("select (sum(debet)-sum(kredit)) as saldo_awal from akt_mutasi where tanggal<? and kde_akun=?", [$TglMulai, $AkunKas]);
        

        $SaldoAwal = [];
        foreach ($HitungSaldo as $k) {
            $SaldoAwal  = $k->saldo_awal;
        }

        $HitungMutasi    = DB::select("select sum(debet) as masuk, sum(kredit) as keluar from akt_mutasi where tanggal>=? and tanggal<=? and kde_akun=?", [$TglMulai, $TglSelesai, $AkunKas]);

        $Masuk = [];
        $Keluar = [];
        foreach ($HitungMutasi as $m) {
            $Masuk = $m->masuk;
            $Keluar = $m->keluar;
        }

        $SaldoAkhir     = $SaldoAwal+$Masuk-$Keluar;

        $ArusKas       = DB::select("select t.tanggal, t.no_bukti, t.kde_akun, a.nma_akun, a.pos_akun, t.debet, t.kredit, t.keterangan from akt_mutasi t, chart_account a where t.kde_akun=a.kde_akun and t.kde_akun=? and t.tanggal>=? and t.tanggal<=? order by t.id", [$AkunKas, $TglMulai, $TglSelesai]);

        return view('admin.lap_akunting.aruskas', compact('ArusKas', 'SaldoAwal', 'Masuk', 'Keluar', 'SaldoAkhir', 'TglMulai', 'TglSelesai'));

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
