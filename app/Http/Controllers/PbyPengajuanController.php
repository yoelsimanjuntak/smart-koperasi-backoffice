<?php

namespace App\Http\Controllers;

use App\Lib\LibTransaksi;
use App\PbyPengajuan;
use Illuminate\Http\Request;
use App\MsAnggota;
use Illuminate\Support\Carbon;
use App\PbyMaster;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Session;

class PbyPengajuanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $PbyPengajuan   = PbyPengajuan::all();
        // $PbyPengajuan = 
        // DB::select("select p.*, a.nama_anggota, m.nama from pby_pengajuan p, pby_master m, ms_anggota a where p.id_pinjaman=m.id and p.id_anggota=a.id order by p.tanggal DESC");
        

        return view('admin.pby_pengajuan.index', compact('PbyPengajuan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Anggota    = MsAnggota::where('status_keanggotaan', 'Aktif')->get();
        $PbyMaster  = PbyMaster::where('kode', '50')->get();

        return view('admin.pby_pengajuan.create', compact('Anggota', 'PbyMaster'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        $this->validate($req,[
            'id_anggota' => 'required',
            'id_pinjaman' => 'required',
            'jumlah' => 'required',
            'jangka' => 'required',
            'keperluan' => 'required',
            'jaminan' => 'required',
        ]);
        $Tgl        = Carbon::now()->format("Ymd");
        $NoPengajuan = LibTransaksi::NoPengajuan(substr($Tgl,-6));
        $Tanggal    = Carbon::now()->format("Y-m-d");
        $IdAnggota  = $req->input('id_anggota');
        $IdPinjaman = $req->input('id_pinjaman');
        $Nominal    = str_replace(".","",$req->input('jumlah'));
        $Jangka     = $req->input('jangka');
        $Keperluan  = $req->input('keperluan');
        $Jaminan    = $req->input('jaminan');
        $UserId = Auth::user()->id;
        $Jenis      = "Pinjaman Tunai";

        $Pengajuan = PbyPengajuan::create([
            'id_anggota' => $IdAnggota,
            'id_pinjaman' =>$IdPinjaman,
            'no_pengajuan' => $NoPengajuan,
            'id_order' => 0,
            'jenis' => $Jenis,
            'no_rek' => '',
            'tanggal'=> $Tanggal,
            'nominal' => $Nominal,
            'jangka' => $Jangka,
            'keperluan' => $Keperluan,
            'jaminan' => $Jaminan, 
            'user_id'=> $UserId,
            'status_pengajuan' => "Menunggu Persetujuan",
        ]);
        Session::flash('flash_message', 'Pengajuan pinjaman telah dibuat');
        return redirect('admin/pengajuan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $PbyPengajuan = PbyPengajuan::findorfail($id);
        return view('admin.pby_pengajuan.show', compact('PbyPengajuan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req)
    {
        $IdPengajuan    = $req->input('id');
        $StsPengajuan   = $req->input('sts_pengajuan');

        $Pengajuan = PbyPengajuan::findorfail($IdPengajuan);
        $Tanggal    = Carbon::now()->format("Y-m-d");
        $Ket        = $req->input('keterangan');

        $Pengajuan->update([
            'status_pengajuan' => $StsPengajuan, 
            'tgl_ubah' => $Tanggal,
            'keterangan' => ($Ket==null ? '' : $Ket)
        ]);

        Session::flash('flash_message', 'Status pengajuan telah diperbarui');
        return redirect('admin/pengajuan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
