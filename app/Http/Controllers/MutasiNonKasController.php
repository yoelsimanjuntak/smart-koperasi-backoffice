<?php

namespace App\Http\Controllers;

use App\ChartAccount;
use App\Lib\CreateJurnal;
use App\Lib\LibAkun;
use App\Lib\LibTransaksi;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class MutasiNonKasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $Memo = DB::select("SELECT t.tanggal, t.no_bukti, t.kde_akun, a.nma_akun, a.pos_akun, t.keterangan, t.debet, t.kredit, t.id FROM chart_account a, akt_mutasi t WHERE t.kde_akun=a.kde_akun and t.jns_mutasi='NonKas' order by t.tanggal DESC");

        // dd($KasMutasi);
        return view('admin.mutasi_nonkas.index', compact('Memo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $AkunKas    = LibAkun::AkunKAS();
        $Akun = ChartAccount::where('kde_akun', '<>',$AkunKas)->get();
        

        return view('admin.mutasi_nonkas.create', compact('Akun'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        $this->validate($req, [
            'akun_debet' => 'required',
            'akun_kredit' => 'required',
            'jumlah' => 'required'
        ]);

        $JnsMutasi  = "NonKas";
        $AkunDebet  = $req->input('akun_debet');
        $AkunKredit  = $req->input('akun_kredit');
        $Nominal    = str_replace(".","",$req->jumlah);
        $Keterangan = $req->input('keterangan');
        $Tgl        = Carbon::now()->format("Ymd");
        $NoBukti    = LibTransaksi::NoBukti(substr($Tgl,-6));
            /// Kas Keluar
        CreateJurnal::AktMutasi($NoBukti, $AkunDebet, $Nominal, 'debet', $Keterangan, $JnsMutasi);
        CreateJurnal::AktMutasi($NoBukti, $AkunKredit, $Nominal, 'kredit', $Keterangan, $JnsMutasi);
        
        Session::flash('flash_message', 'Data Berhasil Ditambahkan');
        return redirect('admin/memorial');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
