<?php

namespace App\Http\Controllers;

use App\PbyJadwal;
use App\PbyMutasi;
use App\PbyRekening;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PbyRekeningController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $PbyRekening    = PbyRekening::where('status','Aktif')->get();
        return view('admin.pby_rekening.index', compact('PbyRekening'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $PbyRekening    = PbyRekening::findorfail($id);
        $PbyMutasi      = PbyMutasi::where('id_norek', $id)->get();
        // dd($PbyMutasi);
        $JdwAngs        = PbyJadwal::where('id_norek', $id)->get();
        $AngsKe         = 
        DB::select("select max(angske) as angs from pby_jadwal where id_norek=? and status='OK'", [$id]);
        $Ke= [];
        foreach($AngsKe as $k){
            if ($k->angs == null){
                $Ke= 1;
            }else{
                $Ke= $k->angs+1;
            }
        }
        $PbyAng = DB::select("select angs_pokok, angs_jasa from pby_jadwal where id_norek=? and angske=?", [$id, $Ke]);
        // dd($PbyAng);

        $Ke= [];
        foreach($PbyAng as $a){
            $NomPokok   = $a->angs_pokok;
            $NomJasa   = $a->angs_jasa;
        }
        

        return view('admin.pby_rekening.show', compact('NomPokok', 'NomJasa','PbyRekening', 'JdwAngs', 'PbyMutasi'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
