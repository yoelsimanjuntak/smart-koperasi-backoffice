<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\JbOrder;
use App\ShuJasaAgt;
use Carbon\Carbon;
use Session;

class PurchasingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $JbOrder       = JbOrder::all();

        return view('admin.purchasing.index', compact('JbOrder'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $JbOrder = JbOrder::findorfail($id);

        return view('admin.purchasing.show', compact('JbOrder'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req)
    {
        // dd($req);
        $IdPengajuan    = $req->input('id');
        $StsPengajuan   = $req->input('sts_pengajuan');
        $KetBatal       = $req->input('ket_batal');
        $Tgl            = Carbon::now()->format('Y-m-d');
        $Order = JbOrder::findorfail($IdPengajuan);
        
        $Order->update([
            'status_order' => $StsPengajuan, 
            'ket_batal' => ($KetBatal==null ? '' : $KetBatal)
        ]);
        /// JIka transaksi selesai hitung keuntungan koperasi
        if ($StsPengajuan=="Selesai"){

            $IdAgt  = $Order->id_anggota;
            $NoTrx  = $Order->no_trx;
            $Hpp  = $Order->hpp;
            $Harga  = $Order->harga;

            $JasaAgt    = $Harga-$Hpp;

            ShuJasaAgt::create([
                'id_anggota' => $IdAgt,
                'tanggal' => $Tgl,
                'no_trx' => $NoTrx,
                'hpp' => $Hpp,
                'harga' => $Harga,
                'nominal' => $JasaAgt
            ]);
            Session::flash('flash_message', 'Pesanan telah selesai');
            return redirect('admin/purchasing');
        }

        Session::flash('flash_message', 'Status Pesanan Telah Diperbarui');
        return redirect('admin/purchasing');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
