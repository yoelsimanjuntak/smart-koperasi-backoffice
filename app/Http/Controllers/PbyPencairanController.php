<?php

namespace App\Http\Controllers;

use App\Lib\LibAkun;
use App\Lib\LibRekening;
use App\Lib\LibTransaksi;
use App\Lib\CreateJurnal;
use App\PbyMaster;
use App\PbyPengajuan;
use App\PbyRekening;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Session;


class PbyPencairanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $PbyPengajuan = PbyPengajuan::where('status_pengajuan', 'Disetujui')->where('no_rek', '')->get();
        
        return view('admin.pby_rekening.pencairan', compact('PbyPengajuan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        $IdPengajuan = $req->input('id');
        $PbyPengajuan   = PbyPengajuan::findorfail($IdPengajuan);

        $NoPengajuan    = $PbyPengajuan->no_pengajuan;
        $IdPinjaman     = $PbyPengajuan->id_pinjaman;
        $IdAnggota      = $PbyPengajuan->id_anggota;
        $NamaAnggota    = $PbyPengajuan->Anggota->nama_anggota;
        $Nominal        = $PbyPengajuan->nominal;
        $Jangka         = $PbyPengajuan->jangka;

        /// get detail produk pinjaman
        $PbyMaster      = PbyMaster::findorfail($IdPinjaman);
        $KodePby        = $PbyMaster->kode;
        $AkunPby        = $PbyMaster->akun_produk;
        $AkunAdm        = $PbyMaster->akun_adm;
        $PersenJasaPby  = $PbyMaster->persen_jasa;
        $PersenByaAdm   = $PbyMaster->bya_adm;
        $Norek          = LibRekening::CreateRekPby($KodePby);
        $TglCair        = Carbon::now()->format('Y-m-d');
        $JthTempo       = Carbon::now()->addMonth($Jangka)->format('Y-m-d');
        
        $NomByaAdm      = $Nominal*($PersenByaAdm/100);
        $UserId         = Auth::user()->id;

        $PbyRek = PbyRekening::create([
            'id_anggota' => $IdAnggota,
            'id_pinjaman' => $IdPinjaman,
            'id_pengajuan' => $IdPengajuan,
            'no_rek' => $Norek,
            'tgl_cair' => $TglCair,
            'jangka' => $Jangka,
            'jth_tempo' => $JthTempo,
            'plafond' => $Nominal,
            'bya_adm' => $NomByaAdm,
            'angske' => 1,
            'saldo_awal_pokok_sys' => 0,
            'saldo_awal_jasa_sys' => 0,
            'saldo_akhir' => $Nominal,
            'user_id' => $UserId, 
            'status' => 'Aktif'
        ]);  
        
        $IdNorek    = $PbyRek->id;

        /// Buat Jadwal Angsuran
        LibRekening::CreateJdwAngs($IdNorek, $PersenJasaPby,$Nominal, $Jangka, $TglCair);

        /// Tandai Pencairan
        $PbyPengajuan->update([
            'no_rek' => $Norek
        ]);

        /// Catat di Akunting
        /// KAS - Kredit || Akun Pinjaman - Debet
        $Tgl        = Carbon::now()->format("Ymd");
        $NoBukti    = LibTransaksi::NoBukti(substr($Tgl,-6));
        $AkunKAS    = LibAkun::AkunKAS();
        $KetCair    = "Pencairan Pinjaman A.n ".$NamaAnggota;
        $KetAdm     = "Pendptan Adm Pinjaman A.n ".$NamaAnggota;


        // Jurnal Pencairan
        CreateJurnal::AktMutasi($NoBukti, $AkunPby, $Nominal, 'debet', $KetCair, 'Pencairan');
        CreateJurnal::AktMutasi($NoBukti, $AkunKAS, $Nominal, 'kredit', $KetCair, 'Pencairan');

        // Jurnal Adm Pencairan
        CreateJurnal::AktMutasi($NoBukti, $AkunKAS, $NomByaAdm, 'debet', $KetAdm, 'AdmPencairan');
        CreateJurnal::AktMutasi($NoBukti, $AkunAdm, $NomByaAdm, 'kredit', $KetAdm, 'AdmPencairan');

        Session::flash('flash_message', 'Pencairan pinjaman telah dilakukan');
        return redirect('admin/pby_rekening');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
