<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SysPerush extends Model
{
    protected $table ='sys_perush';
    protected $fillable = [
        'kde_kantor', 'nma_kantor', 'kota', 'alamat', 'telp', 'hp', 'fax', 'email'
    ];
}
