<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JbOrder extends Model
{
    protected $table ='jb_order';
    protected $fillable = [
        'no_trx', 'tanggal', 'id_anggota', 'id_produk', 'hpp', 'harga', 'pembayaran', 'status_order', 'notes', 'qty', 'ket_batal'
    ];

    public function MsProduk()
    {
        return $this->belongsTo('App\MsProduk', 'id_produk');
    }

    public function PbyPengajuan()
    {
        return $this->belongsTo('App\PbyPengajuan', 'id_order');
    }

    public function Anggota()
    {
        return $this->belongsTo('App\MsAnggota', 'id_anggota');
    }
}
